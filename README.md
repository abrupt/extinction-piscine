# ~/ABRÜPT/ANTHROPIE/EXTINCTION PISCINE/*

La [page de ce livre](https://abrupt.cc/anthropie/extinction-piscine/) sur le réseau.

## Sur le livre

Il ne faut faire aucun effort pour flotter dans la piscine, et plus s’annonce l’extinction, plus on bronze.

Voyage, sois libre, pars à la rencontre de l’autre, découvre qui tu es vraiment.

Mais quand on est libre, quand on voyage, quand on s’accomplit soi-même, on devient du territoire plein.

Et tout autour de nous crève du manque d’espace.

## Sur l'auteur

<a href="http://anthropie.art" target="_blank">anthropie</a> est un collectif d’écriture audiovisuelle pour enfants (sauvages)

<a href="http://anthropie.art" target="_blank">anthropie</a> est un geste en constante augmentation

<a href="http://anthropie.art" target="_blank">anthropie</a> est une cellule esthético-procrastino-insurrectionnelle

<a href="http://anthropie.art" target="_blank">anthropie</a> prône le times new roman, l’anonymat, l’open-source, le DIY, le hack des machines et des esprits

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Zero ([CC ZERO 1.0](LICENSE-TXT)).

La version HTML de cet antilivre est placée sous [licence MIT](LICENCE-MIT).

Elle utilise également les objets 3D créés, sous licence CC-BY-4.0, par [Shalmon](./antilivre-dynamique/public/three/models/building/license.txt) et [Raholder0909](./antilivre-dynamique/public/three/models/duck/license.txt).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
