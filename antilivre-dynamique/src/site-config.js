export const SITE = {
  author: 'Anthropie & AAA',
  title: 'Extinction Piscine | Antilivre | Abrüpt',
  displayTitle: 'Extinction Piscine',
  description: 'Il ne faut faire aucun effort pour flotter dans la piscine, et plus s’annonce l’extinction, plus on bronze. Voyage, sois libre, pars à la rencontre de l’autre, découvre qui tu es vraiment. Mais quand on est libre, quand on voyage, quand on s’accomplit soi-même, on devient du territoire plein. Et tout autour de nous crève du manque d’espace.',
  url: 'https://www.antilivre.org/extinction-piscine/',
  titlelink: 'https://abrupt.cc/anthropie/extinction-piscine/',
  lang: 'fr',
  locale: 'fr_FR',
  warning: 'warning',
  credits: 'Antitexte&nbsp;: anthropie<br>Antimonde&nbsp;: AAA'
};

// // Theme configuration
// export const PAGE_SIZE = 10;
export const THREE_SITE = true;
