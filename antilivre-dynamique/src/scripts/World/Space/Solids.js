import * as THREE from 'three';
import * as SkeletonUtils from 'three/examples/jsm/utils/SkeletonUtils.js';
import World from '../World.js';
import * as TOOLS from '../../tools/tools.js';

export default class Solids extends EventTarget {
  constructor() {
    super();

    this.world = new World();
    this.scene = this.world.scene;
    this.resources = this.world.resources;
    this.time = this.world.time;
    this.debug = this.world.debug;

    this.randomPosMinY = 600;
    this.randomPosMaxY = 800;
    this.randomPosMinYStart = 50;
    this.randomPosMaxYStart = 400;

    // Resource
    this.solidsList = [];

    this.params = {
      name: 'Duck1',
      shape: this.resources.items.duck.scene,
      scale: 3.8
    };

    this.setFlyingObjects(this.params.shape, 99, this.params.scale);
    this.setBackgroundObjects();

    // Debug
    // if (this.debug.active) {
    //   this.debugFolder = this.debug.ui.addFolder('solids');
    // }
    // if (this.debug.active) {
    //   this.debugFolder
    //     .add(this.params, 'name', { Duck1: { shape: this.resources.items.duck.scene, scale: 3.8 }, Duck2: { shape: this.resources.items.duck2.scene, scale: 0.04 } })
    //     .name('Shape')
    //     .onChange((value) => {
    //       this.removeFlyingObjects();
    //       this.setFlyingObjects(value.shape, 99, value.scale);
    //     });
    // }
  }

  setSolid(solid, position, scale) {
    solid.scale.set(scale.x, scale.y, scale.z);
    solid.position.set(position.x, position.y, position.z);
    // solid.position.set(position);
    this.scene.add(solid);
  }

  setFlyingObjects(obj, number, scaleNb) {
    for (let i = 0; i < number; i++) {
      const solid = SkeletonUtils.clone(obj);

      const angle = Math.random() * Math.PI * 2; // Random angle
      const radius = 1 + Math.random() * 200; // Random radius
      const position = {
        // x: TOOLS.randomNb(this.randomPosMin, this.randomPosMax),
        // y: TOOLS.randomNb(this.randomPosStartMax, this.randomPosStartMaxAlt),
        // z: TOOLS.randomNb(this.randomPosMin, this.randomPosMax)
        x: Math.cos(angle) * radius,
        y: TOOLS.randomNb(this.randomPosMinYStart, this.randomPosMaxYStart),
        z: Math.sin(angle) * radius,
      };
      const randomScale = scaleNb;
      const scale = {
        x: randomScale,
        y: randomScale,
        z: randomScale
      };
      this.setSolid(solid, position, scale);
      solid.speedRotation = TOOLS.randombNbFloat(-0.2, 0.2);
      solid.speedPosition = TOOLS.randombNbFloat(0.1, 0.2);
      this.solidsList.push(solid);
      solid.multiplyX = TOOLS.plusOrMinus();
      solid.multiplyY = TOOLS.plusOrMinus();
      solid.multiplyZ = TOOLS.plusOrMinus();
      this.waterPosition = -150;
    }
  }

  setBackgroundObjects() {
    this.shapeBuilding = [this.resources.items.building.scene];
    const positionBuilding = {
      x: 0,
      y: -150,
      z: -300
    };
    const scaleBuilding = {
      x: 50,
      y: 50,
      z: 50
    };

    this.setSolid(SkeletonUtils.clone(this.shapeBuilding[0]), { x: 0, y: -40, z: -300 }, { x: 50, y: 50, z: 50 });
    this.setSolid(SkeletonUtils.clone(this.shapeBuilding[0]), { x: 500, y: -0, z: -400 }, { x: 100, y: 50, z: 50 });
    this.setSolid(SkeletonUtils.clone(this.shapeBuilding[0]), { x: -300, y: -20, z: 300 }, { x: 50, y: 50, z: 50 });
    this.setSolid(SkeletonUtils.clone(this.shapeBuilding[0]), { x: -350, y: -90, z: -150 }, { x: 50, y: 50, z: 50 });
    this.setSolid(SkeletonUtils.clone(this.shapeBuilding[0]), { x: 0, y: -10, z: 400 }, { x: 50, y: 50, z: 50 });
    this.setSolid(SkeletonUtils.clone(this.shapeBuilding[0]), { x: 400, y: 0, z: 0 }, { x: 50, y: 50, z: 50 });
    this.setSolid(SkeletonUtils.clone(this.shapeBuilding[0]), { x: 300, y: -30, z: 300 }, { x: 50, y: 50, z: 50 });
  }

  removeFlyingObjects() {
    for (const solid of this.solidsList) {
      this.scene.remove(solid);
    }
  }

  update() {
    for (const solid of this.solidsList) {
      const time = this.time.elapsed / 1000;
      solid.rotation.x = time * solid.speedRotation;
      solid.rotation.y = time * solid.speedRotation;
      // const solidAngle = this.time.elapsed / 1000;
      // solid.position.x = Math.cos(solidAngle) * (7 + Math.sin(this.time.elapsed * 0.32));
      // solid.position.z = Math.sin(solidAngle) * (7 + Math.sin(this.time.elapsed * 0.5));
      // solid.position.y = Math.sin(this.time.elapsed * 4) + Math.sin(this.time.elapsed * 2.5);

      // if (solid.position.x > this.distanceMovement) {
      //   solid.multiplyX = -1;
      // } else if (solid.position.x < -this.distanceMovement) {
      //   solid.multiplyX = 1;
      // }
      // if (solid.position.z > this.distanceMovement) {
      //   solid.multiplyZ = -1;
      // } else if (solid.position.z < -this.distanceMovement) {
      //   solid.multiplyZ = 1;
      // }

      // solid.position.x += (solid.multiplyX * solid.speedPosition);
      // solid.position.y += (solid.multiplyY * solid.speedPosition);
      // solid.position.z += (solid.multiplyZ * solid.speedPosition);
      // solid.position.x += (solid.multiplyX * (Math.cos(time) * solid.speedPosition) + (1 + Math.sin(time * 0.32)));
      // solid.position.y += ((Math.sin(time) * solid.speedPosition) + (1 + Math.cos(time * 0.5)));
      // solid.position.z += ((Math.sin(time) * solid.speedPosition) + (1 + Math.cos(time * 2.5)));

      solid.position.y -= (0.5 * solid.speedPosition);
      if (solid.position.y < this.waterPosition) {
        solid.position.x = Math.cos(Math.random() * Math.PI * 2) * (1 + Math.random() * 200);
        solid.position.y = TOOLS.randomNb(this.randomPosMinY, this.randomPosMaxY);
        solid.position.z = Math.sin(Math.random() * Math.PI * 2) * (1 + Math.random() * 200);
      }
    }
  }
}
