import * as THREE from 'three';
import { Water } from 'three/examples/jsm/objects/Water.js';
import { Sky } from 'three/examples/jsm/objects/Sky.js';
import World from '../World.js';

export default class Environment {
  constructor() {
    this.world = new World();
    this.scene = this.world.scene;
    this.renderer = this.world.renderer;
    this.resources = this.world.resources;
    this.debug = this.world.debug;

    this.sun = new THREE.Vector3();
    this.sky = new Sky();
    this.water = new Water();

    this.renderTarget;

    // Debug
    if (this.debug.active) {
      this.debugFolder = this.debug.ui.addFolder('environment');
    }

    this.params = {
      background: 0xfa81cb,
      fog: 0xfa81cb,
      water: 0x00ecfa
    };

    this.setWater();
    // this.setSky();
    this.setLight();
  }

  setSky() {
    this.sky.scale.setScalar(450000);
    this.scene.add(this.sky);

    this.skyUniforms = this.sky.material.uniforms;

    this.skyUniforms.turbidity.value = 10;
    this.skyUniforms.rayleigh.value = 2;
    this.skyUniforms.mieCoefficient.value = 0.005;
    this.skyUniforms.mieDirectionalG.value = 0.8;

    this.parameters = {
      elevation: 2,
      azimuth: 180
    };

    const phi = THREE.MathUtils.degToRad(90 - this.parameters.elevation);
    const theta = THREE.MathUtils.degToRad(this.parameters.azimuth);

    this.sun.setFromSphericalCoords(1, phi, theta);

    this.skyUniforms.sunPosition.value.copy(this.sun);
    this.water.material.uniforms.sunDirection.value.copy(this.sun).normalize();

    this.pmremGenerator = new THREE.PMREMGenerator(this.renderer.instance);
    if (this.renderTarget !== undefined) this.renderTarget.dispose();
    this.renderTarget = this.pmremGenerator.fromScene(this.sky);
    this.scene.environment = this.renderTarget.texture;

    // Debug
    // if (this.debug.active) {
    //   this.debugFolder
    //     .add(this.skyUniforms.turbidity, 'value')
    //     .name('turbidity')
    //     .min(0)
    //     .max(20)
    //     .step(0.001);
    //
    //   this.debugFolder
    //     .add(this.skyUniforms.rayleigh, 'value')
    //     .name('rayleigh')
    //     .min(0)
    //     .max(4)
    //     .step(0.001);
    //
    //   this.debugFolder
    //     .add(this.skyUniforms.mieCoefficient, 'value')
    //     .name('mieCoefficient')
    //     .min(0)
    //     .max(0.1)
    //     .step(0.001);
    //
    //   this.debugFolder
    //     .add(this.skyUniforms.mieDirectionalG, 'value')
    //     .name('mieDirectionalG')
    //     .min(0)
    //     .max(1)
    //     .step(0.001);
    // }
  }

  setLight() {
    this.ambientLight = new THREE.AmbientLight('#ffffff', 1); // soft white light
    this.scene.add(this.ambientLight);
    this.sunLight = new THREE.DirectionalLight('#ffffff', 4);
    // this.sunLight.castShadow = true;
    // this.sunLight.shadow.camera.far = 15;
    // this.sunLight.shadow.mapSize.set(1024, 1024);
    // this.sunLight.shadow.normalBias = 0.05;
    this.sunLight.position.set(3.5, 2, -1.25);
    this.scene.add(this.sunLight);

    const canvas = document.createElement('canvas');
    canvas.width = 1;
    canvas.height = 32;

    const context = canvas.getContext('2d');
    const gradient = context.createLinearGradient(0, 0, 0, 32);
    gradient.addColorStop(0.0, '#bb0000');
    gradient.addColorStop(0.5, '#dd0000');
    gradient.addColorStop(1.0, '#ff0000');
    context.fillStyle = gradient;
    context.fillRect(0, 0, 1, 32);
    const skyMap = new THREE.CanvasTexture(canvas);
    skyMap.colorSpace = THREE.SRGBColorSpace;

    const sky = new THREE.Mesh(
      new THREE.SphereGeometry(4000),
      new THREE.MeshBasicMaterial({ map: skyMap, side: THREE.BackSide })
    );
    // this.scene.add(sky);

    // Background
    this.scene.background = new THREE.Color(this.params.background);
    this.scene.fog = new THREE.FogExp2(this.params.fog, 0.0025);
    if (this.debug.active) {
      this.debugFolder
        .add(this.params, 'background', { Rose: 0xfa81cb, Bleu: 0x00ecfa })
        .name('Background Color')
        .onChange((value) => {
          this.scene.background.set(value);
          this.scene.fog.color = new THREE.Color(value);
          // this.scene.fog.set(value);
        });
    }
  }

  setWater() {
    this.waterGeometry = new THREE.PlaneGeometry(10000, 10000);

    this.water = new Water(
      this.waterGeometry,
      {
        textureWidth: 512,
        textureHeight: 512,
        waterNormals: new THREE.TextureLoader().load('three/textures/waternormals.jpg', (texture) => {
          texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        }),
        sunDirection: new THREE.Vector3(),
        sunColor: this.params.background,
        waterColor: this.params.water,
        distortionScale: 3.7,
        // fog: this.scene.fog
        fog: this.scene.fog !== undefined
      }
    );

    this.water.rotation.x = -Math.PI / 2;
    this.water.position.y = -10;

    this.scene.add(this.water);
  }

  update() {
    this.water.material.uniforms.time.value += 1.0 / 60.0;
  }
}
