import * as TOOLS from '../tools/tools.js';

const bg = TOOLS.randomNb(1, 10);

export default [
  {
    name: 'duck',
    type: 'gltfModel',
    path: 'three/models/duck/scene.gltf'
  },
  {
    name: 'building',
    type: 'gltfModel',
    path: 'three/models/building/scene.gltf'
  }
];
