import Header from './Layout/Header';
import World from './World/World';

const menuBtn = document.querySelector('.button--menu');
const menuEl = document.querySelector('.menu');
const canvas = document.querySelector('.world');

const header = new Header(menuBtn, menuEl);
const world = new World(canvas);
