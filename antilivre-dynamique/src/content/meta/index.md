<div class="text">

les rues sont imbibées d'alcool

mais les caméras ont mouillé nos silex

même le néolithique a l'air moderne

plus personne ne sait faire du feu

à part les nuages

qui crament

#thisisfine

</div>


<div class="text">

là

on traîne sur le trottoir

en attendant le bus

&nbsp;

il fait froid

les arbres tirent un peu la gueule

on est encore mouillé·es

&nbsp;

une pellicule de chlore sur la peau

les poumons dilatés que le vent laboure

tout l'iceberg amer

du corps qui vient de nager

&nbsp;

ce matin

les fleuves étaient fermés

alors on est allé·es à la piscine

&nbsp;

et plus on glandait

sur les dalles blafardes

dans nos maillots en polymère

plus on remarquait

combien les piscines municipales

ressemblent au capitalisme tardif

#swimmingwithsharks

</div>

<div class="text">

1\) il ne faut faire aucun effort

pour flotter dans la piscine

&nbsp;

2\) on a toustes appris à nager

dès l'enfance

&nbsp;

3\) il est déconseillé

de courir autour de l'eau

&nbsp;

4\) ce qui a de la valeur

reste cadenassé aux vestiaires

&nbsp;

5\) il n'y a pas beaucoup de place

dans les jacuzzis

&nbsp;

6\) le chlore ne nique pas les yeux

tant qu'on les garde fermés

&nbsp;

7\) il n'est pas autorisé

d'incendier le bâtiment

&nbsp;

là où ce bus nous emmène

l'incendie prendra peut-être

en tout cas

on a enfilé nos k-way ignifuges

#wardrobessentials

</div>

<div class="text">

on débarque un peu sans prévenir

ça te dérange pas ?

&nbsp;

c'était galère de les semer

on n'a pas vraiment eu le choix

on t'a perdu·e de vue

quand les flics nous ont dispersé·es

&nbsp;

t'inquiète pas

personne ne nous a vu·es entrer

&nbsp;

est-ce qu'on peut passer la soirée chez toi ?

&nbsp;

ça fait du bien de s'affaler

dans ton salon étrange

avec son désordre adolescent

son aquarium encrassé

son odeur

moitié kebab

moitié vernis à ongles

ses guirlandes lumineuses

que t'as volées chez Ikea

pour fabriquer un genre de cirque

&nbsp;

au coin du canapé

des cadavres de bouteilles

des cadavres de bougies

même le cadavre d'une souris

qu'on croirait plongée

dans un sommeil réparateur

#creepycute

</div>

<div class="text">

de ta fenêtre

on saisit la ville d'en haut

on réussirait presque

à rebâtir une distance

avec les néons

les trottoirs

les commissariats

au moins une distance intérieure

#skyscraper

</div>

<div class="text">

l'odeur du guêpier

plane dans la métropole

mais perd de sa terreur

quand elle atteint le cinquième étage

&nbsp;

il est rassurant ton appartement

même si on sent bien

que les années sont passées

sans qu'aucune thune ne ruisselle

sur le cocon que t'as construit

&nbsp;

les anxiolytiques aussi

ont sculpté ta grotte

d'une croûte de chimie dure

qui s'épaissit avec le temps

c'est la texture amère

que prend la précarité

quand elle se pétrifie

#homedesign

</div>

<div class="text">

t'es sûr·e que toi ça va ?

t'es arrivé·e y'a longtemps ?

t'as l'air encore un peu sous le choc

&nbsp;

viens

on refait une de ces soirées

où on essaie de trouver encore

quelque chose à se raconter

à chaque fois

ça nous fait du bien

#healingjourney

</div>

<div class="text">

tu le trouves pas étrange

ce concert de voix

qui répondent à l'urgence

en valorisant la complexité ?

en affectant la retenue ?

&nbsp;

peut-être

que c'est nous qui vieillissons

nous qui ressentons

plus sensiblement qu'avant

combien pèse sur la nuque

l'injonction à la maturité

à la parcimonie

à la pacification

#wise

</div>

<div class="text">

tu rigoles

mais est-ce que ce serait

vraiment si insensé

qu'à la sénilité des écosystèmes

en phase terminale

on réponde par la colère

évidente et incendiaire

de l'adolescence ?

#wiser

</div>

<div class="text">

t'as raison

tout se ressemble

&nbsp;

on s'est trompé·es tout à l'heure

on voulait entrer chez toi

mais on a débarqué chez les voisin·es

et on n'a pas su faire la différence

&nbsp;

l'univers est misérable et homogène

homogène comme la misère

misérable parce que la marchandise

nous a rendu·es homogènes

#samesamebutdifferent

</div>

<div class="text">

alors tout d'abord

nique les marchandises

ce qui a une valeur

c'est les histoires

&nbsp;

mais ensuite

les histoires

on a du mal à les trouver

&nbsp;

tu te souviens

cet après-midi

ce moment face à la foule

t'avais le mégaphone dans les mains

et soudain

plus rien à dire

#findyourpurpose

</div>

<div class="text">

t'y es pas pour grand-chose

en tant que génération

on n'arrive pas à se raconter

&nbsp;

faut qu'on fasse le deuil

de toute parole

absolument commune

faut qu'on lutte chaque jour

contre l'idéal uniforme

d'une paire de cordes vocales partagée

qui ferait vibrer la gorge

de toute la génération

#thevoice

</div>

<div class="text">

on dit pas ça pour rappeler

que la diversité des opinions

c'est super chouette

&nbsp;

depuis à peu près deux cents ans

les dominants répètent en boucle

qu'il faut des points de vue divergents

pour garantir la pluralité

du débat démocratique

#makeup

</div>

<div class="text">

et

depuis à peu près deux cents ans

les dominants continuent de dominer

alors bon

on va y aller mollo

avec la pluralité

&nbsp;

se couper la tête

dans des débats sans fin

c'est devenu une valeur

c'est devenu un jeu

c'est devenu l'équilibre absurde

comme condition de l'agression

toujours reconduite

&nbsp;

conclusion locale de l'argumentaire :

la génération démocratisée s'autoguillotine

&nbsp;

mais rappel du premier argument :

rêver la monophonie

c'est se crever les yeux

avec le cutter de l'idéalisme

&nbsp;

alors déduction semi-générale :

on sait pas vivre les yeux ouverts

sans se couper la tête

#hangover

</div>

<div class="text">

et là

on amène la conclusion définitive

qui sera notre théorie principale :

&nbsp;

partout

c'est la grosse piscine

et tout le monde nage

#poolparty

</div>

<div class="text">

la parole

qu'on va tenter ensemble

en plein cœur du paradoxe

c'est celle des enfants

de la classe moyenne défaitiste

avec leurs sourires ubérisés

leurs empathies qui défaillent

et leurs suffisances occidentales

#pov

</div>

<div class="text">

l'histoire qu'on va un peu réécrire

et un peu inventer

c'est celle de la génération extinction

c'est celle de la génération piscine

et à la fin on va rêver un peu

en laissant la parole

à la génération premier degré

&nbsp;

c'est comme ça qu'on voudrait vivre

au premier degré

t'as déjà vu quelqu'un

pleurer au second degré ?

#nofilter

#forreal

</div>

<div class="text">

t'es pas certain·e d'avoir un avenir

c'est une angoisse que tu partages

avec le reste de notre génération

on n'est pas certain·es d'avoir un avenir

&nbsp;

on n'est pas certain·es

que des livres d'histoire

parleront un jour de nous

parce qu'on n'est pas certain·es

qu'il reste encore assez d'avenir

pour écrire des livres d'histoire

#soldout

</div>

<div class="text">

le problème

c'est que ce monde est tout neuf

alors qu'on a encore

le visage de nos parents

&nbsp;

chaque matin

on revit

la vieille sensation du cycle

on ouvre les yeux

on sent le soleil

sur nos visages

tout est radieux

tout est à faire

#trusttheprocess

</div>

<div class="text">

surtout l'été

surtout dans les villes

surtout quand t'as des thunes

surtout quand ta peau

est blanche comme l'insouciance

#blessed

</div>

<div class="text">

rien

à aucune intersection

d'aucun des cercles

rien

n'a l'air condamné

&nbsp;

tout pulse

une énergie

impossible à épuiser

et tout court à l'épuisement

&nbsp;

tout court à l'épuisement

parce qu'on existe

c'est le principe actif

d'une angoisse très particulière

la certitude

que sa propre présence est dévastatrice

dans un univers où tout sonne

comme une injonction

à exister plus fort

#monsterenergy

</div>

<div class="text">

voyage

sois libre

pars à la rencontre de l'autre

découvre qui tu es vraiment

choisis ce plan d'épargne avantageux

pour les moins de vingt-cinq ans

ne doute jamais

de la nécessité

de ta présence

#youonlyliveonce

</div>

<div class="text">

mais quand on est libre

quand on voyage

quand on s'accomplit soi-même

on devient du territoire plein

et tout autour de nous

crève du manque d'espace

&nbsp;

alors chaque été

après une bonne année de taf

on remplit les mêmes plages

pour essayer de faire le vide

#letitgo

</div>

<div class="text">

toi tu fais comment

quand t'arrives plus

à faire le vide ?

&nbsp;

c'est sûr

faudrait qu'on réussisse à imaginer

autre chose que la catastrophe

mais notre génération

n'a plus aucune confiance

en son imagination

&nbsp;

la preuve

on n'a plus beaucoup d'ami·es

on n'a même plus d'ami·es imaginaires

#fakenews

</div>

<div class="text">

interrogé·es par un institut de sondage

60 % des 18-30 ans déclarent

« ne pas avoir de vrai·es ami·es »

&nbsp;

à la question

« comment envisagez-vous l'avenir ? »

83 % ont répondu

« on va devoir se débrouiller seul·es »

#lucidity

</div>

<div class="text">

chez toi

le monde est calme

&nbsp;

juste en glandant sur les coussins

ou en errant sur instagram

on atteint par le creux

une simulation

de paix intérieure

#truth

</div>

<div class="text">

mais on se demande quand même

pourquoi t'as claqué ton salaire

dans ce tapis de course ridicule

#innergrowth

</div>

<div class="text">

on a ramené des chips

elles sont dans le sac à dos noir

qui traîne près de la table

&nbsp;

tu voudrais pas aller chercher

quelque chose à boire ?

&nbsp;

depuis la cuisine

un air glacial

s'engouffre dans le salon

&nbsp;

c'est la porte du frigo

qui s'est cassée

quand t'as essayé de l'ouvrir

#broke

</div>

<div class="text">

du frigo béant

le blizzard souffle si fort

que le froid commence

à nous mordre

sous les ongles

&nbsp;

doucement

nos mains bleuissent

leur chair se fige

&nbsp;

il faudrait allumer la hotte

pour qu'elle aspire

les flocons qui volent

au milieu des poêles

#winterwonderland

</div>

<div class="text">

le givre se condense

et dessine des glyphes

sur le carrelage de la cuisine

#abstract

</div>

<div class="text">

on croirait presque entendre

la respiration du gel

on croirait presque

qu'une énergie autonome

anime son expansion

#hauntedhouse

</div>

<div class="text">

il est lent

il recouvre les murs

il atteint la fenêtre

il opacifie le soleil

&nbsp;

quelques minutes à peine

et une neige épaisse

tapisse le parquet

&nbsp;

regarde comme les gerçures

ont vallonné nos paumes

regarde nos métacarpes à nu

nos empreintes digitales

illisibles

#nailporn

</div>

<div class="text">

des stalactites de glace

fixées au plafond

perlent quelques larmes

sur le simili-cuir du canapé

où nous nous blottissons

comme une portée si frêle

que l'hiver lui crevassera les os

le cœur

et tout le désordre intérieur

#petsofinstagram

</div>

<div class="text">

le sang du rongeur mort

gèle brutalement

et dans les ténèbres

du couloir

on entend quelque chose rugir

#primalfear

</div>

<div class="text">

pourquoi tu craques

toutes ces allumettes ?

pourquoi tu les avales

quand elles s'apprêtent à mourir ?

#youarewhatyoueat

</div>

<div class="text">

il n'y a plus rien à faire

juste rester prostré·es

sous les plaids premier prix

et se ronger

les doigts

&nbsp;

ou s'en servir

de ses doigts

pour dessiner des formes

sur le cellophane de buée

qui recouvre

toutes les vitres du salon

&nbsp;

c'est marrant

à travers les lettres

que tu traces

on devine la ville

qui bruit dehors

&nbsp;

t'as vu comme il fait sombre ?

&nbsp;

c'est pas normal

on est en plein été

on est en plein jour

mais on va s'y habituer

tu verras

on s'habitue à tout

#winteriscoming

</div>

<div class="text">

<div class="datation">

<em>décembre 536</em>

<em>276.67 ppm de CO<sub>2</sub></em>

&nbsp;

si tu colles bien ton nez

au verre de la paroi

tu verras les reflets

devenir des formes

les immeubles au loin

qui commencent à danser

&nbsp;

puis s'effacent

sous un ciel cendre

carrément noir certains jours

même si la lueur du soleil

ne disparaît jamais complètement

&nbsp;

on n'est plus au 21^e^ siècle

on est en 536

&nbsp;

la tectonique est déchaînée

les volcans des deux hémisphères

vomissent pendant plusieurs mois

&nbsp;

une poussière lourde

cimente le ciel

l'atmosphère devient si opaque

que le soleil ne passe plus

&nbsp;

la planète entière

recommence sa glaciation

&nbsp;

au coin des rues

tapissées de paille

de grêle violente

et d'animaux décagés

tu les entends partout

les prophètes et les serfs

qui annoncent la clôture

du règne humain

&nbsp;

partout on murmure

que la lumière s'est éteinte

à jamais

&nbsp;

dans ce nouveau monde

la nuit est la règle

et tu dois réapprendre

la mécanique des plantes

réapprendre à te nourrir

en sachant cultiver

ce qui pousse sans soleil

&nbsp;

dans le pétrole des villes

dans les nuits absolues

qui couvent les campagnes

contrairement à ce que t'aurais pu croire

l'obscurité est accueillante

&nbsp;

les rugissements dans le noir

ont un air presque doux

la terreur primale

disparaît peu à peu

et tu t'habitues

&nbsp;

tu verras

on s'habitue à tout

&nbsp;

la poussière volcanique

abolit les saisons

&nbsp;

cette année-là

il neige

en plein mois d'août

&nbsp;

la température des rivières

augmente brutalement

tu t'y baignes avec plaisir

&nbsp;

certaines espèces de poissons

disparaissent

d'autres s'adaptent

deviennent translucides

et froides

&nbsp;

durant les premiers mois de glaciation

plusieurs émeutes

agitent la planète

&nbsp;

les palais se vident

&nbsp;

face à l'extinction

on ne comprend plus bien

l'intérêt qu'on trouvait autrefois

à obéir

&nbsp;

tout le monde

a emménagé dans la nuit

alors plus personne

ne paie d'impôts

&nbsp;

partout sur les lèvres

du vieil ordre politique

juste des gerçures

et la sécheresse du silence

&nbsp;

juste des peaux mortes

qui se détachent

avec lenteur

et fertilisent un monde

que tu foules

sereinement

&nbsp;

au Japon

durant le printemps 536

un édit impérial

décrète que la nourriture

vaut plus que les perles

&nbsp;

en Scandinavie

les gouvernants paniqués

incendient les réserves d'or

pour apaiser leurs dieux

&nbsp;

à Byzance

pendant l'hiver de juillet

la noblesse échange ses demeures

contre des charrettes

et part sur les routes

&nbsp;

partout sur les lèvres

du vieil ordre de la valeur

le sel de la mutation

érode les chairs

&nbsp;

si tu glisses un œil

derrière les remparts à l'abandon

tu verras que les palais

se remplissent à nouveau

d'un monde éclaté

végétal et curieux

qui reprend possession

des salons

des chambres

et des écuries

&nbsp;

mais le temps passe

et avec une pointe de tristesse

tu constates que la lumière

perce à nouveau les nuages

&nbsp;

le soleil revient

et avec lui les polices

comme toujours

&nbsp;

elles vident par la force

les palais occupés

sans pouvoir effacer

qu'autre chose

un bref instant

y a trouvé une forme

&nbsp;

les vents finissent

par disperser la cendre

par faire oublier

que les dérèglements de température

ont bien failli abolir l'autorité

et congédier la valeur

&nbsp;

la nuit accueillante se dissipe

le soleil trône à nouveau

et les empires s'en sortent

&nbsp;

pour le moment

</div>

</div>

<div class="text">

odeur de musc

confort animal

on voudrait encore

rester blotti·es contre toi

&nbsp;

pour le moment

on coagule

en un ventre unique

on s'agglomère

autour d'un rêve commun

#comfy

</div>

<div class="text">

ce rêve

c'est la rupture de l'hibernation

&nbsp;

comme le soleil

lèche nos visages

une moiteur nouvelle

mouille nos organes

&nbsp;

les rayons gamma rebondissent

sur le parquet couvert de neige

&nbsp;

fais attention à ton visage

couvre-le de tes mains

ne laisse pas tes yeux brûler

#enlightenment

</div>

<div class="text">

tout brille

tout est aveuglant

il est devenu difficile

de faire la différence

entre la totalité du réel

et une barre de néon

#retrofuture

</div>

<div class="text">

restons infrarouges

c'est trop tôt encore

pour parvenir à penser

&nbsp;

les tuyaux des radiateurs

explosés par le gel

pissent dans l'appartement

des jets à haute pression

&nbsp;

il faut tenter nos premiers pas

à travers les geysers

tituber ensemble

au moins jusqu'à la cuisine

&nbsp;

maintenant

la cafetière est sur le feu

la température monte

#countdown

</div>

<div class="text">

et on regarde le café brûler

comme on regarde avec indifférence

brûler les pays

où poussent les caféiers

&nbsp;

c'est tout l'ordinaire

un malaise après l'autre

qui se reconstruit

#coffeelovers

</div>

<div class="text">

il n'est pas si tard

l'hibernation aura été

curieusement brève

on a l'impression

d'avoir laissé fondre

des années entières

#timelapse

</div>

<div class="text">

attention

en réchauffant tes mains

n'approche pas ton pelage des flammes

&nbsp;

tu ne l'avais pas vu

ton pelage

&nbsp;

chaque centimètre de nos peaux

s'est couvert d'une fourrure épaisse

c'est pour ça qu'on avait chaud

nos organismes réagissent à l'hiver

#furryfriends

</div>

<div class="text">

on devra prendre soin

de ce manteau confortable

et vu comme il se densifie

il faudra le brosser souvent

#curlyroutine

</div>

<div class="text">

tu voudrais pas

nous couper les cils ?

ils sont beaucoup trop longs

on pourrait presque

se les ranger derrière l'oreille

&nbsp;

regarde-toi dans la fenêtre

t'as l'air d'un animal

mais ça te va bien

t'es même assez photogénique

avec tes lunettes de soleil

debout à contempler la ville

sans quitter ton sourire

de grand mammifère triste

#beautyandthebeast

</div>

<div class="text">

les radiateurs crevés

continuent de s'épandre

le niveau de l'eau monte

la température grimpe aussi

#countdown

</div>

<div class="text">

alors on casse

la pointe d'une stalactite

et on la regarde

flotter dans le café noir

un dernier frappuccino

avant l'extinction

#baristalife

</div>

<div class="text">

le frappuccino à la main

tu as pris l'habitude d'être libre

c'est une routine que tu partages

avec le reste de notre génération

on a pris l'habitude d'être libres

&nbsp;

et quand on sait

que l'habitude

est une forme douce et inconsciente

de captivité

c'est là

sans doute

qu'il aurait fallu s'inquiéter

&nbsp;

le pouvoir vertical

qu'ont connu nos parents

a creusé le bassin

et installé les chaises longues

où on profite du soleil

#itsatrap

</div>

<div class="text">

maintenant le bassin est rempli

et le pouvoir vertical

entre dans la phase programmée

de son obsolescence

&nbsp;

aujourd'hui

on se marre pas mal

quand on entend

des discours trop écrits

&nbsp;

quand on voit les treillis militaires

les hélicoptères ridicules

les robes colorées des magistrats

toute cette ruine d'autorité agonique

qui cherche à rester un spectacle

pour retarder

sa propre disparition

#retromovie

</div>

<div class="text">

et quand on rit

de ces ruines d'autorité

tout en lignes

en angles droits

en frontispices

quand on se fout de leur gueule

on ne voit pas qu'on nage

une brasse coulée et triste

#trapped

</div>

<div class="text">

aujourd'hui

le pouvoir est devenu liquide

et on nous a inventé

plein de nouvelles natations

#neverendingstory

</div>

<div class="text">

alors plus personne

ne sait faire du feu

parce que tout le monde

boit la tasse

à part le soleil

qui incendie les nuages

#cheers

</div>

<div class="text">

ta transpiration est agressive

elle nous stresse

arrête de courir

sur ce tapis roulant

là tu pues le vieux fauve

qui a trop chassé dehors

et ça colle pas du tout

avec ton sourire idiot

de gosse enfermé·e

&nbsp;

tu t'es toujours pas réchauffé·e ?

&nbsp;

certains jours on se demande

où tu trouves ces calories

que tu brûles en ligne droite

sans jamais avancer

#cardio

</div>

<div class="text">

certains jours on sait plus

ce qu'il faut affronter

mais on reste stoïques

et on se bat contre tout

#eyeofthetiger

</div>

<div class="text">

feu dans toutes les directions

feu sur soi

et feu sur la planète entière

&nbsp;

c'est qu'on a déclaré la guerre

au soleil

&nbsp;

c'est une course

à qui dévorera la planète

en premier

#competitiveeating

</div>

<div class="text">

les astronomes

les plus optimistes

estiment que le soleil

consumera notre monde

dans sept milliards d'années

&nbsp;

alors

en tant qu'espèce

on accélère

pour y arriver avant

#eventplanner

</div>

<div class="text">

on est l'outsider

sur un côté du ring

contre la montre

tête dans le guidon

&nbsp;

le soleil c'est la force tranquille

il attend que la gravité

amène la terre dans ses bras

pour brûler infiniment

#love

</div>

<div class="text">

on est légion

on peut cramer la Terre

le soleil est colossal

il doit attendre

et douter

&nbsp;

alors c'est vrai

qu'on pourrait s'accorder une pause

regarder l'extinction-soleil

comme une promesse lointaine

vivre avec la mort cosmique

comme horizon tolérable

&nbsp;

mais on ne sera plus jamais

capables de faire une pause

&nbsp;

longtemps

on s'est couché·es de bonne heure

mais plus maintenant

maintenant on ne dort plus

#spacecake

</div>

<div class="text">

il y a toujours une bonne raison

de rester éveillé·es

un truc à regarder

un truc qui vient remplir

le tonneau percé

de nos récepteurs dopaminergiques

&nbsp;

le pouvoir liquide

nous a rendu·es accros à l'addiction

accros à la quasi-noyade

comme horizon nécessaire

#sunrise

</div>

<div class="text">

le pouvoir liquide

nous a filé des brassards

juste assez gonflés

pour oublier

l'horizon tolérable

de l'extinction-soleil

&nbsp;

le pouvoir liquide

organise sans se presser

notre extinction-piscine

#sunset

</div>

<div class="text">

débranche le tapis de course

le salon continue de se remplir

l'eau nous arrive aux genoux

et on ne va pas risquer le court-circuit

#interiorlighting

</div>

<div class="text">

tu sens

comme le sel brûle les lèvres ?

c'est l'iode de nos sueurs

et c'est celle de l'océan

&nbsp;

les canalisations explosées

inondent l'appartement

le niveau de l'eau monte

#countdown

</div>

<div class="text">

on n'aura pas le temps

de plastiquer les fontaines

&nbsp;

quitte à finir trempé·es

c'est l'occasion

d'inventer quelque chose

#betterworld

</div>

<div class="text">

on pourrait se rouler un joint

mais il nous faudrait un siège

un peu plus imperméable

que cette épave de canapé

pour le fumer tranquillement

&nbsp;

il y a encore

les matelas gonflables

qu'on avait pris ce matin

en allant à la piscine

parce que les fleuves

étaient fermés

&nbsp;

on pourrait aussi

commander des sushis

essayer comme on peut

d'habiter l'inondation

#scubadiving

</div>

<div class="text">

voilà

là on est bien

on plane sur les matelas

on se laisse rebondir

entre les quatre murs

de ton appartement

on pratique la navigation d'intérieur

&nbsp;

et plus la weed

nous bronze le cerveau

plus on rêvasse en crawlant

de la cuisine au salon

et plus la ligne de flottaison grimpe

plus l'appartement

remonte à la surface

&nbsp;

le canapé

les plantes artificielles

les bouteilles de shampoing

tout un étrange continent postindustriel

qui dérive en pirate

sans jamais aller bien loin

#wander

</div>

<div class="text">

regarde

si on se penche un peu

on voit de gros poissons marins

qui nagent

l'air intrigué

autour de ton aquarium

et à travers la fenêtre

qui donnait sur la ville

d'autres poissons

nous regardent regarder

toujours ton aquarium

#metavers

</div>

<div class="text">

on sonne à la porte

les sushis viennent d'arriver

et ce serait cool de manger

devant quelque chose

#foodgasm

</div>

<div class="text">

heureusement

la télé flotte

comme un genre de bouée

arrimée au sol

par son câble d'alimentation

elle diffuse en boucle

un documentaire animalier

#wildlifelovers

</div>

<div class="text">

l'eau va bientôt

atteindre le plafond

et nous écraser contre lui

#dontlookup

</div>

<div class="text">

touche tes clavicules

juste sous ton pelage

tu sens cette fente inhabituelle ?

ce truc spongieux

et curieusement frais ?

&nbsp;

maintenant

on a des branchies

#mermaidsarereal

</div>

<div class="text">

on peut immerger nos visages

inspirer sans entrave

nos corps filtrent l'oxygène

ils relâchent des chapelets de bulles

&nbsp;

on est souples

fluides malgré la fourrure

prêt·es à explorer

le chaos humide

des fonds marins

#underwaterselfie

</div>

<div class="text">

laisse tes branchies

se gonfler

se vider

comme des siphons

et recommence

recommence encore

&nbsp;

nos poitrines respirent

avec l'obstination

de ces poissons migrateurs

qui survivent aux typhons

&nbsp;

une pieuvre curieuse

s'approche de la télé

ces eaux sont trop froides

pour ses tentacules fragiles

qui préfèrent se lover

contre la chaleur de l'écran

&nbsp;

le rythme des images

change la couleur

de sa peau

même la texture

de son corps

#glitchart

</div>

<div class="text">

cette odeur de décomposition

qui filtre à travers

la puanteur des algues

c'est le cadavre du rongeur

qui dérive avec nous

&nbsp;

il faut qu'on évite

ses tripes humides

ses entrailles mouillées

où habitent les champignons

les bactéries

#viral

</div>

<div class="text">

de là où on est

on ne voit qu'un corps bouffi

mais dans le secret

de la forgerie moléculaire

le bacille entame sa lutte

elle améliore son logiciel

se divise en séquences

devient une machine

à produire des versions

#update

</div>

<div class="text">

la salive

lui monte aux lèvres

le dégel

lui rend l'appétit

elle dévore les rayons du soleil

nourrit son empire

de chairs gonflées

jusqu'à installer ses colonies

dans nos cœurs

dans les trois cœurs de la pieuvre

et dans ceux des insectes

#stayhome

</div>

<div class="text">

<div class="datation">

<em>novembre 1340</em>

<em>281.07 ppm de CO<sub>2</sub></em>

&nbsp;

et les insectes infectés

courent sur les rats

qui courent sur les pavés

du jeune 14^e^ siècle

&nbsp;

nos matelas ont dérivé

jusque dans les eaux territoriales

du port de Catane

&nbsp;

de là où on est

l'agitation du vivant est perceptible

à la quantité de marchandises

qu'il empile

&nbsp;

l'échange réinvente sans cesse

de nouvelles manières

de se rendre meurtrier

&nbsp;

cette année-là

les flottes marchandes de Gênes

importent en Sicile

quelques rats malades

&nbsp;

on n'imagine pas encore

que la peste noire

deviendra « la mort noire »

et tuera

près d'un tiers de l'Europe

&nbsp;

alors que les premiers bubons

explosent sur des cadavres italiens

la nouvelle de l'infection se propage

comme une épidémie

&nbsp;

et bientôt l'infection

devient pandémie

&nbsp;

tout tremble autour de nous

le siècle a la peau qui frissonne

&nbsp;

les royaumes réagissent

ils mettent en place

des procédures de quarantaine

bouclent les ports

massacrent les rats

&nbsp;

les guérisseurs et les guérisseuses

redoublent de ferveur

pour soigner les maîtres et les maîtresses

&nbsp;

depuis leurs territoires glacés

les gouvernances nordiques

croient avoir le temps

elles se préparent

à protéger leurs puissants

de la peste continentale

&nbsp;

en Norvège

de nouvelles lois sont édictées

elles interdisent aux paysans et aux paysannes

d'entrer dans les enceintes protégées

&nbsp;

on triple le nombre de médecins

affectés au roi et à sa cour

on sécurise les ports

on entame la construction

d'immenses murailles

autour des maisons les plus riches

&nbsp;

quand les années 1340 finissent

la peste touche l'Angleterre

&nbsp;

quelques mois plus tard

un bateau quitte le port de Londres

en direction du Danemark

avec à son bord

une cargaison de laine

un médecin

un dispositif de quarantaine

et un couple de rats

caché dans la double coque

&nbsp;

pendant quelques jours

les rats et les marins

habitent en harmonie

se nourrissent mutuellement

leurs environnements parasitaires

entrent en symbiose

&nbsp;

on observe les symptômes

sur un premier marin

qu'on met en quarantaine dans la soute

puis un deuxième

un troisième

&nbsp;

très vite

le bateau n'est plus habité

que par deux rats solitaires

qui ignorent tout du pilotage

mais disposent

d'un immense garde-manger

et ils commencent à se reproduire

pour tuer le temps

&nbsp;

le navire dérive sur la mer du Nord

pendant plusieurs semaines

il échoue finalement sur la côte

non loin du port de Bergen

au sud de la Norvège

&nbsp;

adossé·es à un rocher

quelque part sur la plage

on regarde les jeunes rats

fouler la terre ferme

pour la première fois

de leur courte vie

&nbsp;

leurs parents

ont connu cette sensation

avant le déluge

et ils les regardent

tendrement

s'éloigner vers la ville

&nbsp;

les noblesses nordiques

ne sont pas prêtes

&nbsp;

les rats se moquent

de leurs remparts

la classe possédante

subit de plein fouet la pandémie

80 % de l'élite meurt

&nbsp;

les historien·nes les plus optimistes

estiment que la peste noire

a décimé

un quart de la population norvégienne

certain·es disent un tiers

&nbsp;

nombre de terres

campagnes

bras de mer

plateaux montagneux

sont laissés sans gouvernance

&nbsp;

alors la gouvernance

s'organise d'elle-même

&nbsp;

sous le coup de la nécessité

certaines communautés

abolissent le principe de propriété

et le territoire se recompose

&nbsp;

on crée des communes

on édicte des lois libertaires

on établit le droit coutumier

de l'allemannsretten

ou "liberté d'errer"

encore en vigueur de nos jours

stipulant que le droit des humains

à bénéficier de la nature

prévaut sur la propriété privée

&nbsp;

parmi les nobles qui survivent

beaucoup doivent renoncer

à dominer

&nbsp;

l'extinction du peuple

ne permet plus

de lever un impôt suffisant

pour leur épargner le travail

&nbsp;

partout dans le pays

les nobles retournent aux champs

&nbsp;

mais le temps passe

et tu remarques les bubons

qui s'effacent de tes bras

la peau sous ton pelage

qui retrouve la santé

&nbsp;

la bactérie s'éloigne

avec l'air moqueur

d'un volcan qui s'éteint

&nbsp;

il suffira de quelques années

pour que le vivant

relance le cycle

de l'accumulation

et de la rareté organisée

&nbsp;

quelques années à peine

avant que les nobles

ne retrouvent leurs donjons

à l'abri des pandémies

&nbsp;

pour le moment

</div>

</div>

<div class="text">

la télé déconne

encore une latte de joint

&nbsp;

litre après litre

l'appartement inondé se vide

et on a un dîner à finir

&nbsp;

c'est notre tour de manger

laisse la barquette

voguer sur l'eau

elle finira par croiser

la trajectoire de nos flotteurs

&nbsp;

merde

elle dérive vers le couloir

et on est trop défoncé·es

pour rattraper les sushis au saumon

qui remontent le courant

#mainstream

</div>

<div class="text">

plus le niveau de l'eau baisse

plus on discerne les murs

violemment lézardés

tout le papier peint qui pèle

le monde après le déluge

&nbsp;

les dangers du grand large

sont derrière nous

on va bientôt pouvoir

rouvrir la fenêtre

&nbsp;

fais attention

ton matelas se dégonfle

il s'est empalé sur les oursins

c'est ça qu'on entend siffler

en retrouvant la terre ferme

&nbsp;

nous aussi

on touche le fond

on retrouve un peu

le sens de la gravité

#lifestyle

</div>

<div class="text">

à sécher nos pelages

en nous secouant comme ça

on a l'air de chiens mouillés

#wetfurryfriends

</div>

<div class="text">

la marée a emporté l'aquarium

les plantes en plastique

et ta carte d'identité

&nbsp;

elle a laissé derrière elle

une étendue grouillante

où nos matelas traînent

comme des épaves

fluos et ridicules

un peu anachroniques

au milieu des ordures

des mollusques égarés

des bouteilles sans message

&nbsp;

t'as vu cette canette de coca ?

elle bouge toute seule

sur ton tapis roulant

un bernard-l'hermite

l'a prise pour coquille

#tastethefeeling

</div>

<div class="text">

c'est le truc le plus drôle

qu'on ait vu de la journée

mais ça laisse

un grand vide à l'intérieur de l'estomac

faut penser à autre chose

&nbsp;

l'océan a laissé le salon

en chantier

mais il a l'air plus fertile

&nbsp;

le roulis des vagues

a sculpté une caverne

curieusement habitable

#beforehistory

</div>

<div class="text">

tu te souviens

du jour où t'as réalisé

que l'humain était capable

de rendre un bout du territoire

parfaitement inhabitable ?

&nbsp;

nous on s'en souvient

on était enfants

c'était pas vraiment un choc

on l'a métabolisé

comme une évidence

nucléaire

qu'on n'a plus jamais questionnée

#selfbelief

</div>

<div class="text">

viens t'asseoir avec nous

mais regarde où tu mets les pieds

le plancher capitule

sous la pression des coraux

un jeune couple de crabes

trimballe même son exosquelette

sur ce qu'il reste

de ton tapis

#workout

</div>

<div class="text">

les larmes te montent aux yeux ?

alors faut fumer

une dernière latte

&nbsp;

pleure pas comme ça

regarde les crabes

ils cahotent parmi les ruines

ils nagent dans le bonheur

#urbex

</div>

<div class="text">

la télé déconne toujours

et ce trou dans ton ventre

plein à craquer

de poisson cru

c'est pas seulement la weed

&nbsp;

même quand on n'est pas défoncé·es

la vidéo et la réalité

sont devenues un espace

homogène

à l'intérieur duquel on teste

des scénarios prédéterminés

#boomerang

</div>

<div class="text">

c'est vrai

qu'on vit encore assez bien

la réduction croissante

de nos palettes émotionnelles

#goodvibesonly

</div>

<div class="text">

mais quand on en arrive au stade

où tout le film nous indiffère

on sait plus

si on est solides émotionnellement

ou si on est juste devenu·es

des figurines interchangeables

qui tapent des siestes à onze heures

#beautysleep

</div>

<div class="text">

c'est assez inédit

comme forme de tristesse

l'extinction de l'espèce

la dissipation du réel

&nbsp;

solastalgie

ou éco-anxiété

même les psychiatres

ont inventé des mots

pour dire la position foireuse

où on a rangé la psyché

de toute une génération

qui compense le spectacle

de l'autodestruction

par un afflux excessif

de stress prétraumatique

#mindset

</div>

<div class="text">

si tu nous regardes bien

certains soirs

tu verras juste d'habiles junkies

qui savent se frayer

un chemin cynique

dans la banqueroute générale

de sérotonine

#traderlife

</div>

<div class="text">

alors quand on se défonce

pour arrêter d'être tristes

et qu'on finit

tristes et défoncé·es

on comprend

que la dépression hivernale

survivra au réchauffement climatique

&nbsp;

on comprend

qu'on traînera encore longtemps

dans nos villes de smog

où le concept de non-fumeur

est devenu une abstraction rassurante

&nbsp;

attaché·es au mât

malgré le naufrage

la tête juste assez émergée

pour écouter d'autres sirènes

que celles des keufs

#oceansounds

</div>

<div class="text">

la tête juste assez immergée

pour entendre

les milliers d'injonctions

à devenir soi-même

dans les strictes limites

de la zone industrielle

&nbsp;

les générations avant la nôtre

ont décrété

que la métaphysique était vide

et qu'il fallait se battre

pour le réel

#okboomer

</div>

<div class="text">

notre génération a décrété

que le réel était vide

&nbsp;

alors on s'est dépêché·es

de passer des diplômes en communication

pour organiser la mise en langage

du précipice insoutenable

#hashtag

</div>

<div class="text">

pour oublier que la différence

entre un bureau et une barricade

c'est 90 degrés d'inclinaison

#doityourself

</div>

<div class="text">

la décomposition des algues

fige un genre de prairie

qui repousse

en touffes herbeuses

jusqu'à recouvrir le plancher

#cottagecore

</div>

<div class="text">

la plage

prend des allures bucoliques

et l'eau condense

&nbsp;

il nous faudrait un linge sec

quelque chose

qui nous couvre le visage

ou au moins les yeux

&nbsp;

la fumée est lourde

l'humidité étouffante

elle irrite les branchies

qui nous collent aux clavicules

&nbsp;

on dirait un peu

le brouillard lacrymogène

qu'on s'est mangé tout à l'heure

&nbsp;

ça y est

on te voit plus du tout

tout est englouti

tout est opaque

#gloomyday

</div>

<div class="text">

tu nous entends encore ?

essaie de suivre

le son de nos voix

tu sens cette énergie étrange

qui grésille dans ton cou ?

&nbsp;

ouvre la bouche

laisse les ultrasons

s'échapper de ta gorge

ils rebondissent sur les murs

ils cartographient l'espace

&nbsp;

l'écholocalisation

nous aide à habiter la brume

l'air autour de nous

comme une fine toile tressée

fait vibrer nos Wi-Fi intérieures

ton appartement est minuscule

mais avec les ricochets hertziens

qui nous jaillissent de la bouche

il a l'air immense

#palace

</div>

<div class="text">

l'intensité des ondes

détaille dans nos cerveaux

les masses

les volumes

et les distances

en d'étranges informations sensibles

qu'on peine encore à décoder

&nbsp;

on perçoit presque

les fluides qui sursautent

dans les tuyaux de l'immeuble

et les corps de tes voisin·nes

qui doivent muter aussi

&nbsp;

on localise même

une nappe huileuse

cent mètres sous terre

peut-être du pétrole

une fortune endormie

sous les parkings de la ville

#makeitrain

</div>

<div class="text">

t'entends ?

quelque chose bêle dans le couloir

quelque chose émet de l'amour

quelque chose signale sa position

au reste du troupeau

&nbsp;

quand tu nous parles

en émettant des ondes

ta voix a l'air autotunée

t'es devenu·e une star

rupestre et radioactive

piégée dans une machine à fumée

avec les deux pieds dans l'herbe

#greenthumb

</div>

<div class="text">

on pourrait presque découper

de gros cubes de tourbe

et allumer un feu

au milieu du salon

on sera mieux au chaud

&nbsp;

regarde nos reflets

dans la fenêtre couverte de mousse

moitié ovipares

moitié locataires endetté·es

moitié fauves

moitié déchets nucléaires

&nbsp;

et puis ouvre-la

la fenêtre

tu sens ce vent chaud ?

il annonce l'orage

alors laisse ton reflet

s'en aller librement

#xoxo

</div>

<div class="text">

<div class="datation">

<em>mai 1789</em>

<em>278.57 ppm de CO<sub>2</sub></em>

&nbsp;

arrête de fixer bêtement l'horizon

et regarde

où tu mets les pieds

&nbsp;

les cultures que tu foules

abritent une vie entière

des insectes étranges

qui commencent à transpirer

&nbsp;

au nord du continent

les volcans s'éveillent

à nouveau

leur cendre refroidit

et fige la stratosphère

&nbsp;

avec un sourire de vacarme

les nuages venus du nord

s'apprêtent à ouvrir

une parenthèse politique

&nbsp;

ils entament le grand dérèglement

&nbsp;

l'hiver 1787 est trop doux

&nbsp;

sans la croûte de gel meurtrière

les nuées de parasites subsistent

s'aventurent vers les pousses

laissent fleurir les mauvaises herbes

&nbsp;

l'œil des paysan·nes de France

se teinte d'inquiétude

&nbsp;

le printemps s'acharne

plusieurs coups de chaleur

détruisent les blés

&nbsp;

les récoltes précoces sont trop faibles

et des millions d'enfants sans bec

raclent la terre

picorent les insectes

&nbsp;

on attelle quand même les animaux

quelques charrettes

pour monter à la capitale

&nbsp;

quand la campagne

commence à brûler

c'est toujours une bonne idée

de monter à la capitale

&nbsp;

sur la route

observe comme tout le territoire

est strié de champs

de forêts domestiques bien alignées

&nbsp;

tout le territoire d'Ancien Régime

suit sa propre géométrie

hyperlisible pour les autorités

&nbsp;

de sa fenêtre

le regard du roi

embrasse un monde

conçu pour lui

de sa fenêtre

il constitue

la visibilité de ses sujets

&nbsp;

mais à l'été 1788

la fenêtre du roi explose

et avec elle

toutes les vitres du palais

&nbsp;

les tableaux se trouent

les membres amputés des statues

tombent en silence dans les jardins

comme les jarrets mal anesthésiés

des animaux à l'abattoir

&nbsp;

ce n'est pas encore le peuple

c'est un orage de grêle

qui dévaste le pays

&nbsp;

regarde où tu mets les pieds

ne marche pas en chaussettes

les 11 749 verrières de Rambouillet

sont en morceaux

&nbsp;

après l'orage de grêle

le prix du pain monte

les récoltes de l'année suivante

sont pires encore

&nbsp;

pourtant

dans sa lucidité proverbiale

de vieux riche idiot

louis XVI autorise

l'export du blé

et les possédant·es

les plus riches

s'en donnent à cœur joie

&nbsp;

à la fin du printemps 1789

les estomacs vides

de blé mort ou vendu

se sont emplis d'insoumission

&nbsp;

tout le territoire d'Ancien Régime

est une manifestation du corps royal

&nbsp;

derrière la douceur des champs

on voit le gant de dieu

et dans le gant

la main du monarque

intransigeante

&nbsp;

quand les récoltes sont bonnes

c'est le roi qu'on acclame

&nbsp;

depuis des siècles

le cycle qui fait la vie

et la subsistance commune

dispose d'un garant royal

signifie une autorité rassurante

&nbsp;

tout se trouble

puis tout s'effondre

l'orage dans sa grande liberté

pompe ses watts

dans la faim

devient foudre insurrectionnelle

&nbsp;

il signe la mort de la sûreté

la nécessité de l'inconnu

l'imprévision

engagée de force

jusqu'à déchirer le cycle

de la répétition politique

&nbsp;

même les réchauffements

même les dérèglements

les plus attendus

conservent cette étrange aptitude

à susciter la surprise

&nbsp;

à la fin de l'été 1789

Paris brûle

et le sang bleu des nobles

réchauffe enfin la terre

qui cicatrise

&nbsp;

pour le moment

</div>

</div>

<div class="text">

on est bientôt

au bout de nos peines

le vent commence

à disperser la brume

mais l'orage plane

#cloudscape

</div>

<div class="text">

la fenêtre ne ferme plus

on l'entend claquer

presque aussi fort que nos dents

&nbsp;

les papillons s'évaporent

les ailes des mouettes

battent sans avancer

le bernard-l'hermite

dans sa canette de coca

s'est envolé comme une feuille

#lonelyboy

</div>

<div class="text">

les murs commencent à grincer

sous la pression du vent

l'horizon se brouille

un morceau du plafond se décroche

&nbsp;

ça y est

on est au cœur de la tempête

&nbsp;

comment tu fais

pour rester stoïque ?

toujours barricadé·e

impossible à atteindre

&nbsp;

mets-toi à l'abri

l'appartement tout entier valse

ce qui était vissé cède

ce qui était collé tombe

&nbsp;

les casseroles s'envolent

de la cuisine au salon

et le contenu des étagères

se fracasse sur le plancher

les jeunes plantes

qui s'installaient

entre les lattes

sont déracinées

&nbsp;

tu marches jusqu'à la porte

tu espères t'enfuir

mais en l'ouvrant

tu ouvres à la tempête

qui te repousse violemment

&nbsp;

un premier mur grince

sous la pression de l'air

le crépi se décolle

et embarque avec lui

toute l'isolation

tout le ciment que le vent écartèle

comme un agrume dérisoire

#rollercoaster

</div>

<div class="text">

un autre mur est emporté

éventré plutôt

mais le squelette des parpaings

survit au choc

&nbsp;

rejoins-nous sous le canapé

et ferme les yeux

&nbsp;

ne rien voir

c'est encore le plus sûr

&nbsp;

attendons que ça passe

&nbsp;

voilà

&nbsp;

on dirait que le vent se calme

&nbsp;

on dirait qu'il relâche son étreinte

pour tourner autour du bâtiment

&nbsp;

tu peux rouvrir les yeux

&nbsp;

lève la tête

il n'y a même plus de toit

tout l'espace est démuré

toute la ville peut nous voir

&nbsp;

l'œil du cyclone

nous fixe sans cligner

#dontblink

</div>

<div class="text">

restons à l'abri sous le canapé

bien au centre de la pièce

le tourbillon aspire nos corps

vers les bords de la plateforme

et plus aucune barrière

ne nous sépare du vide

#mindthegap

</div>

<div class="text">

on s'écraserait

cinq étages plus bas

&nbsp;

assieds-toi

ne marche pas

si près du gouffre

&nbsp;

passe une main dans ton dos

tu sens

ces deux moignons infantiles ?

&nbsp;

du sang

des plumes

qui percent la fourrure

juste là

entre tes omoplates

&nbsp;

tes ailes ne sont pas prêtes

&nbsp;

s'il te plaît

reviens vers nous

&nbsp;

il nous faudrait un bout de tôle

du plexiglas opaque

du carton

&nbsp;

n'importe quoi

tant qu'on peut s'y cacher

construire une forteresse

même éphémère

rester imperceptibles

éviter de s'exposer

pas trop se débattre

#peacefulwarrior

</div>

<div class="text">

on finit trop souvent

par se débattre

à l'intérieur des murs

qu'on a abattus

#defeatedwarrior

</div>

<div class="text">

le canapé ne suffit pas

va falloir renverser l'appartement

&nbsp;

empiler les étagères

refixer les trucs les uns aux autres

la porte du frigo aux lattes du lit

les volets à ce tableau ignoble

puis étirer encore des tapis

boucher les ouvertures

tendre par-dessus tout ça

le rideau imperméable de ta douche

et on aura un abri décent

en plein cœur du salon

#homesweethome

</div>

<div class="text">

voilà

on est protégé·es du vent

protégé·es des regards

&nbsp;

ça fait du bien

d'être couché·es

contre ton corps

d'écouter les grenouilles

#ASMR

</div>

<div class="text">

ça fait toujours ça

les pluies violentes

quand on est au chaud

&nbsp;

et la nuit

sans douceur

commence à tomber

#seeyoutomorrow

</div>

<div class="text">

c'est beaucoup mieux

la tempête nous assiège

mais au moins on est protégé·es

des regards de la ville

#blindtest

</div>

<div class="text">

dans ce gros désordre

qui n'aura plus jamais

une gueule d'appartement

tu ne te sens pas observé·e

&nbsp;

c'est un soulagement

que tu partages

avec le reste de notre génération

on ne se sent pas observé·es

#magicmirror

</div>

<div class="text">

et pour s'assurer

qu'on barbote tranquillement

le pouvoir liquide

s'est trouvé des maîtres-nageurs

avec des capes d'invisibilité

#pottermore

</div>

<div class="text">

paradoxalement

l'objectif à court terme

c'est toujours d'être visible

les politiques doivent être visibles

les entreprises doivent être visibles

les produits doivent être visibles

être visible est même devenu

un job à part entière

#showyourbeauty

</div>

<div class="text">

les rois ne voyaient pas grand-chose

quand un peu par hasard

ils attrapaient un·e criminel·le

ils l'affichaient

dans le tonnerre du spectacle

pendaisons dans les champs

écartèlements sur les marchés

décapitations devant le château

&nbsp;

les états modernes

sont des entreprises qui ont réussi

alors ils ont pris leurs distances

avec les orages

#waterproof

</div>

<div class="text">

ils ont caché les corps

tué à l'abri des regards

érigé des prisons

où l'on se sait observé·e

ils ont disséminé

partout et en plein jour

ces petites caméras

noires et sphériques

fondé le contrôle

sur la surveillance

comme feeling

#vintage

</div>

<div class="text">

maintenant

les cyberpouvoirs

tuent le feeling

&nbsp;

l'observation disparaît

&nbsp;

alors

soudain angoissé·e

l'observé·e se montre

#showmustgoon

</div>

<div class="text">

le feeling sous contrainte

ne suffit plus

pour nous dicter notre conduite

&nbsp;

on s'est habitué·es à tout voir

habitué·es à être vu·es

on hurle

dans tous les canaux de streaming

pour accéder enfin au silence

&nbsp;

à chaque mutation du contrôle

on découvre une nouvelle esquive

une autre manière de voir

une autre manière d'être vu·e

&nbsp;

chaque résistance

invente une soumission

mais aucune goutte d'eau

ne fait déborder la piscine

#fiftyshadesofgrey

</div>

<div class="text">

il doit être à peu près minuit

plus grand-chose ne donne l'heure

tu sais te repérer

avec les étoiles ?

nous non plus

#lost

</div>

<div class="text">

au moins il reste des clopes

&nbsp;

on pourrait s'aventurer

en dehors du refuge

pour en partager une

&nbsp;

on voudrait voir un peu

quelle gueule il a

le monde dehors

#openspace

</div>

<div class="text">

la tempête est partie

le déluge a laissé derrière lui

une bruine diffuse

&nbsp;

les grenouilles qu'on entendait

à l'intérieur de l'abri

en fait elles sont des dizaines

elles règnent sans partage

sur nos marécages suspendus

&nbsp;

impossible de fumer cette clope

sans marcher dans l'eau

sans écarter ces roseaux

qui nous écorchent les jambes

#neverquit

</div>

<div class="text">

si t'avais des bottes de pluie

on sauterait dans les flaques

comme les batraciennes créatives

qu'il faudra bien devenir

si on veut continuer

d'habiter ton appartement

&nbsp;

c'est absurde

on dit ton appartement

mais c'est plus qu'un rectangle

de béton sans parois

au sommet d'un immeuble

#roomwithaview

</div>

<div class="text">

une tourbière

de quarante mètres carrés

posée en haut d'une tour

avec une cabane au milieu

&nbsp;

devant nous le néant

même les ultrasons que l'on émet

s'égarent dans l'horizon

&nbsp;

tant de satellites

que le ciel s'étoile

de lueurs un peu électriques

un peu minérales

&nbsp;

quelques goélands viennent cueillir

les grenouilles dissidentes

qui nous sautent entre les jambes

ils doivent nicher pas loin

quand les oiseaux mangent

quand les oiseaux font l'amour

le sol tremble un peu

&nbsp;

marche pas aussi près du bord

s'il te plaît

on te l'a déjà dit

tout est glissant

si tu veux t'approcher du vide

accroupis-toi et rampe

nos ailes galèrent à pousser

&nbsp;

par contre

nos bouches ont muté

#duckface

</div>

<div class="text">

d'un coup de langue extensible

ultrarapide et hyperréaliste

on gobe les moustiques

et les mouches

comme les batraciennes créatives

qu'il a bien fallu devenir

&nbsp;

voilà

on a dévoré les lucioles

on entend mieux les drones

ils sont devenus

plus faciles à repérer

plus faciles à abattre

#huntingseason

</div>

<div class="text">

c'est vrai que l'appartement éventré

découpe quatre frontières nettes

mais les limites de la piscine

se sont évaporées

avec les limites du monde

et il n'y a pas de limites

à l'économie de la donnée

parce que le monde

n'a pas de limites

#freemium

</div>

<div class="text">

depuis environ

la période où on est né·es

le monde a commencé

à produire une copie de lui-même

toujours perfectible

&nbsp;

et la copie n'a pas de limites

#follow4follow

</div>

<div class="text">

le monde a toujours été

un empilement de variables

mais on avait pas encore envisagé

à l'échelle de sapiens

qu'un alignement de nombres

puisse devenir habitable

&nbsp;

c'est qu'il n'existe aucun phénomène

qui ne puisse devenir donnée

&nbsp;

plus la surface du réel

sera dure à habiter

plus on posera des capteurs

qui peuvent donner la vie

plus il fera bon vivre

à l'intérieur de la copie

#instagood

</div>

<div class="text">

et comme il faut bien

gérer les données

la totalité des phénomènes

l'ensemble de la vie comme moments

est devenu gérable

#dailyroutine

</div>

<div class="text">

alors là

c'est la grosse euphorie

c'est la piscine olympique

&nbsp;

partout un désir ardent

de devenir un objet

ou juste le reflet d'un objet

sur une paroi du cloud

partout un désir ardent

mais jamais incendiaire

de devenir silicone

#shadows

</div>

<div class="text">

comme une intuition collective

à se former en gestionnaire

pas pour maîtriser la copie

la copie restera souveraine

juste y collaborer

accéder au bonheur

de construire ensemble

toujours plus de données

bosser en Californie

ou dans les Californies du coin

&nbsp;

adopter des algorithmes

comme animaux de compagnie

devenir du même coup

animaux de compagnie

pour algorithmes

#petlovers

</div>

<div class="text">

tu sais plus comment vivre

sans que tout soit augmenté

c'est une hallucination que tu partages

avec le reste de notre génération

on sait plus comment vivre

sans que tout soit augmenté

&nbsp;

mais tu sais

la copie ne flotte pas dans les airs

ses pieds sont en argile

&nbsp;

des groupes d'individus

trop mobiles pour être décalqués

pourraient nager entre ses jambes

et remonter chacun des fils

de la grande toile de danger

qu'a tissé l'Occident

&nbsp;

apnée entre les câbles

poumons électroniques

cagoules multicolores

et briquets dans les poches

&nbsp;

un court-circuit incendiaire

et tout le coton du *cloud*

fumerait comme la laine

des moutons électriques

#bedtimesstories

</div>

<div class="text">

un court-circuit incendiaire

et toute la Silicon Valley

exhalerait une douce odeur

de silicium fondu

&nbsp;

ça vraiment

ce serait un spectacle

qui mériterait

de devenir une société

#happyending

</div>

<div class="text">

<div class="datation">

<em>juin 2019</em>

<em>410.53 ppm de CO<sub>2</sub></em>

&nbsp;

les goélands de ce fleuve

qui ne s'appelle pas encore la Seine

nichent sur les pierres

dans les herbes hautes

qui peuplent les marécages

&nbsp;

des marécages qui nous précèdent

des marécages qui étaient là

cinq mille ans avant notre naissance

&nbsp;

de là où ils sont

cinq mille ans avant notre naissance

les goélands voient bien la plaine

le fleuve qui la cisèle

et nourrit toute la horde

&nbsp;

la plaine est vide

parfaitement habitable

et des générations d'oiseaux

y appareillent sans entrave

&nbsp;

puis la plaine est envahie

les premières habitations

agglutinées le long du fleuve

hébergent des primates innocent·es

qui courent après les lion·nes

et finissent presque toujours

par se faire dévorer

&nbsp;

les goélands continuent la chasse

mais leur cœur est lourd

en gaélique

gouelan signifie « pleurer »

&nbsp;

quand des foyers

s'allument dans les cabanes

les goélands s'approchent

nichent sur les toits

et gardent leurs œufs au chaud

&nbsp;

au fil des siècles

les oiseaux tentent de coexister

déplacent encore leur nid

réinventent la prédation

&nbsp;

les cabanes changent de dimension

et les donjons de pierre

érigés sur la plaine

deviennent des perchoirs sans danger

&nbsp;

depuis leurs hauteurs fortifiées

les goélands regardent

calmement

les volées de flèches

qui passent en contrebas

les boulets de fonte

qui s'échouent dans le fleuve

tout le grondement des guerres

mutilant la plaine

&nbsp;

puis les usines

commencent à fumer

et intoxiquent les oisillons

&nbsp;

alors leurs parents trouvent refuge

sous les toits prolétaires

et commencent à théoriser

la fin de la coopération

&nbsp;

tout au long du siècle

les balles perdues

tirées sur les manifestant·es

qui occupent les fabriques

explosent par erreur

des œufs innocents

&nbsp;

les nids sont encore déplacés

encore reconstruits

toujours temporaires

toujours dérisoires

mais toujours autonomes

et toujours défendus

&nbsp;

finalement la capitale commence

à produire une copie d'elle-même

et la copie de la capitale

n'a pas de limites

&nbsp;

les opérateurs de la copie

sont les drones de la police

&nbsp;

ils commencent

à scanner les carrefours

à filmer les impasses

à déranger l'atmosphère

où habitent les oiseaux

&nbsp;

alors toute l'espèce décide

que la paix a trop duré

&nbsp;

les goélands comprennent

que les drones s'envolent

d'étranges nids électroniques

lovés dans les commissariats

&nbsp;

les drones s'appellent tous Elsa

Engins Légers de Surveillance Aérienne

&nbsp;

les goélands ne peuvent pas voir

les télépilotes qui guident Elsa

dans ses vols d'observation

&nbsp;

les télépilotes voient tout

mais jamais personne

n'a vu un·e télépilote

&nbsp;

Elsa survole les rues

Elsa explore les impasses

fabrique la carte

des meutes actionnées

des agitations sous contrôle

&nbsp;

Elsa est légère

c'est dans son nom

elle fluidifie

&nbsp;

les régulateurs

aiment déréguler

mais les gestionnaires

détestent la congestion

&nbsp;

Elsa décongestionne

&nbsp;

alors quelque chose vient toucher

ce qui touchait sans toucher

quelque chose de très énervé

une colère de plumes

&nbsp;

les goélands se cachent sous les toits

guettent les drones

et quand une proie passe

s'envolent d'un même élan

&nbsp;

le vent porte leurs membres

ils sont plus souples

plus habiles que les engins

leurs becs détruisent les câbles

et leurs pattes arrachent

les ailes ridicules des robots

&nbsp;

les goélands organisé·es

ont entrepris méthodiquement

de crever les yeux d'Elsa

&nbsp;

de là où t'es

tu ne vois que les chutes

les carcasses en polymère

de fonctionnaires électroniques abattus

que d'autres fonctionnaires ramassent

avec une moue dépitée

et dans le regard

une once de regret budgétaire

&nbsp;

quand le matin viendra

Elsa ne rentrera pas au nid

vaincue par les géants

qui l'empêchent de marcher

&nbsp;

pour le moment

</div>

</div>

<div class="text">

regarde

encore un drone qui vole

#safari

</div>

<div class="text">

tu sais

cette résistance ovipare

contre les robots policiers

elle a défrayé les journaux parisiens

pendant quelques mois

mais on a embelli l'histoire

&nbsp;

depuis la police a développé

des drones antigoélands

qui diffusent des ultrasons

pour faire fuir les oiseaux

et ça les rend fous

&nbsp;

ils n'auront pas le temps de réagir

on peut se raconter ce qu'on veut

l'espèce évolue moins vite

que les dispositifs sécuritaires

#slowmotion

</div>

<div class="text">

c'est normal

que tu sois fatigué·e

il doit être trois heures du matin

on devrait essayer de dormir

#restless

</div>

<div class="text">

tu recommences

&nbsp;

on t'a déjà demandé

on t'a déjà supplié en fait

de t'éloigner du bord

&nbsp;

on sait ce que t'as en tête

on n'a pas envie de le dire

et t'as pas envie d'en parler

mais il va falloir en parler

#voidcore

</div>

<div class="text">

en tout cas

il va falloir arrêter de se taire

&nbsp;

on va plus pouvoir ignorer

ce visage impassible

que t'affiches en permanence

et qui colle pas à la situation

&nbsp;

on est désolé·es

on est vraiment désolé·es

tu sais pas quoi dire

ça oui

on peut le comprendre

&nbsp;

t'as vu

ces points lumineux là-bas

on dirait des feux de camp

peut-être d'autres meutes

peut-être d'autres appartements

ou d'anciennes maisons bourgeoises

effondrées mais lumineuses

&nbsp;

tu penses que les autres

iels ont muté comme nous ?

&nbsp;

si tu veux avoir la réponse

il faudrait que demain

tu sois encore parmi nous

&nbsp;

t'as vraiment plus assez de curiosité

pour encore une journée sur terre ?

&nbsp;

s'il te plaît

te lève pas encore une fois

reste assis·e

peut-être

qu'on n'est pas assez clair·es

ce qu'on voudrait

c'est t'entourer d'un amour

qui nous torde ensemble

et torde avec nous

tout le spectre du réel

#gravity

</div>

<div class="text">

ce qu'on voudrait

c'est regarder avec toi

le monde au premier degré

&nbsp;

on a lu quelque part

même si c'est probablement faux

que 1968 serait l'année

ayant connu

le taux de suicide le plus bas

en Europe occidentale

#motivationalquote

</div>

<div class="text">

les émeutes rachètent l'avenir

mais c'est pas seulement les émeutes

les émeutes c'est très accessoire

c'est l'espace

pour inventer autre chose

la perspective

de la fin des jours présents

#reset

</div>

<div class="text">

on voit bien que c'est trop vague

pour gonfler de sens

le réel autour de toi

&nbsp;

d'accord

oui tu peux te lever

pardon

on te suppliera plus

&nbsp;

on te croit

on te prend au sérieux

on va venir avec toi

&nbsp;

s'asseoir tout près du bord

et balancer nos jambes

au-dessus du vide

#onedge

</div>

<div class="text">

on t'a vu te faire attraper

cet après-midi

au milieu des fumigènes

&nbsp;

ça fait un moment

qu'on a compris

la lassitude dans ton visage

&nbsp;

ton sursis va sauter

et cette fois

tu vas prendre du ferme

&nbsp;

on peut fuir ensemble

on est prêt·es à faire ça

encore quelques heures

et nos ailes auront poussé

#wingsuit

</div>

<div class="text">

t'es sûr·e ?

si c'est vraiment ce que tu veux

on t'en empêchera pas

on détournera pas les yeux

ça on te le promet

&nbsp;

mais on trouve quand même

que tu devrais hésiter plus que ça

&nbsp;

est-ce qu'on peut

te serrer contre nous ?

&nbsp;

oui on pleure

mais ça tu vas devoir vivre avec

enfin

partir avec

&nbsp;

c'est l'image que t'emmèneras

en plongeant dans le vide

#tearsintherain

</div>

<div class="text">

on peut pas te prendre au sérieux

tout en retenant nos larmes

c'est politiquement incompatible

&nbsp;

c'est ça le premier degré

l'impression définitive

de ne plus être en train de jouer

#gamechanger

</div>

<div class="text">

bien sûr

&nbsp;

après 1968

on a redécouvert

les vertus du second degré

comme armure

comme maillot de bain

&nbsp;

et depuis

posé·es sur nos matelas gonflables

flottant sans histoire

un soda à la main

on garde nos lunettes de soleil

pour bien fixer

le feu des nuages

#swag

</div>

<div class="text">

et plus on réalise

la préfabrication générale

plus notre second degré se développe

&nbsp;

il y a corrélation des deux courbes

&nbsp;

tout nous amuse

tout nous divertit

#lol

</div>

<div class="text">

même l'histoire

on a du mal

à la prendre au sérieux

&nbsp;

elle est devenue

une image-objet

un spectacle-machine

comme le reste

&nbsp;

même nos dépressions

même nos burn-outs

on a du mal

à les prendre au sérieux

on les soigne à la plage

on fait des pauses

qui ont le goût du kérosène

on continue à jouer

on joue à s'épuiser

et on soigne l'épuisement

avec les outils des épuiseurs

#workplacewellness

</div>

<div class="text">

si seulement

on arrivait à faire un break

avec le besoin de faire un break

#haveakitkat

</div>

<div class="text">

les vieux antagonismes de classes

n'ont pas disparu

ils ont juste été modélisés

dans un jeu vidéo permanent

et comme dans n'importe quel jeu

pour gagner la partie

il faut intérioriser l'algorithme

#actorstudio

</div>

<div class="text">

notre génération apprend

la grande compétition du monde

en faisant l'expérience des règles

qui gouvernent la piscine

&nbsp;

notre génération

apprend que ces règles

sont arbitraires

et sans fondement

qu'on peut les utiliser

avec cynisme et brutalité

pour aller plus loin dans la partie

#workhardplayhard

</div>

<div class="text">

tout est devenu un jeu

qu'on a trop conscience de jouer

et plus on a conscience de jouer

plus notre second degré se développe

&nbsp;

il y a corrélation des deux courbes

&nbsp;

on fait semblant

d'être solides

semblant d'en avoir rien à foutre

mais c'est pour survivre

à l'arbitraire

#newskill

</div>

<div class="text">

là s'ouvre l'ironocène

l'époque du second degré général

reconductible et illimité

&nbsp;

dans notre superpiscine

c'est notre superpouvoir :

il n'existe rien

qu'on ne puisse tourner en dérision

#trollface

</div>

<div class="text">

« vis dans ton monde

joue dans le nôtre »

c'est le slogan qu'ils ont choisi

pour vendre la playstation

mais ils auraient pu choisir

« joue dans ton monde,

vis dans le nôtre »

et tout aurait été parfaitement identique

#whatever

</div>

<div class="text">

le plus gros défi

de notre génération

c'est de garder son sérieux

c'est de prendre à nouveau

le monde au premier degré

#trynottolaugh

</div>

<div class="text">

alors c'est vrai que c'est chiant

d'avoir grandi

en détestant nos possibles

&nbsp;

mais quand on te voit

marcher vers nous

depuis le bord du précipice

où t'as refusé de sauter

&nbsp;

quand on te voit éteindre ta clope là

et sourire

on a envie de pleurer

et cette fois

c'est ni la beuh

ni les lacrymos

c'est vraiment la joie

c'est l'instant

&nbsp;

t'as déjà vu quelqu'un

pleurer au second degré ?

#nofilter

#forreal

</div>

<div class="text">

finalement

on n'aura pas dormi une seconde

le soleil se lève

#welcomeback

</div>

<div class="text">

tu vois

le monde a changé de gueule

et tu ne tousses plus

les lacrymos ont fini

par déserter ta gorge

#healthy

</div>

<div class="text">

maintenant

il te faut une bonne douche

juste pour nettoyer

les derniers résidus du gaz irritant

et préparer ton envol

tes ailes ont presque fini de pousser

&nbsp;

regarde la ville entamer sa mue

toute la surface du réel

redevenue intéressante

#goldenhour

</div>

<div class="text">

on dirait bien

que tous les appartements

ont connu le même sort

que celui qui t'appartenait

&nbsp;

quand on sera descendu·es en ville

ou envolé·es dans les airs

on ne reconnaîtra jamais le tien

on ne retrouvera plus jamais

le chemin de la maison

#digitalnomad

</div>

<div class="text">

plus rien n'est spectaculaire

le monde n'est pas effondré

l'humanité n'est pas éteinte

il n'y a qu'Hollywood

pour faire fortune

sur l'apocalypse

sur le spectacle de la fin

et la paralysie du public

&nbsp;

tout lutte simplement

pour changer sans trop souffrir

tout lutte simplement

pour réduire la souffrance

c'est la composante

la plus révolutionnaire

du vivant mutilé

&nbsp;

c'est le dernier moment

qu'on voudrait partager

qu'on voudrait vivre à tes côtés

juste regarder le monde

au premier degré

sortir de l'obsession

du retournement

prendre au sérieux

la possibilité du retournement

#hashtaghashtag

</div>

<div class="text">

et sous le soleil de ce matin

c'est plus facile que d'habitude

&nbsp;

un truc dans nos membres

qui évacue les toxines

un truc qu'on pourrait appeler

le contraire de la passivité

un truc qui résiste

à l'image de la transformation générale

à l'extinction comme spectacle

#detox

</div>

<div class="text">

un truc qui aide à envisager

la journée de combat

comme un horizon satisfaisant

&nbsp;

tiens

c'est étrange

on croirait entendre

quelqu'un frapper à la porte

#cliffhanger

</div>

<div class="text">

on a halluciné

il n'y a plus de porte

&nbsp;

mais il y a bien une silhouette

qui se découpe

dans la lueur du levant

raide comme une matraque

figée dans ce qu'il reste

de ton appartement

&nbsp;

c'est un officier de police

et le flingue à la ceinture

de son uniforme antiémeute

est blanc de calcaire

#ancientstatue

</div>

<div class="text">

son gros bouclier en plexiglas

est couvert de larves

ses bottes trop lourdes

s'enfoncent dans la tourbière

son uniforme trop serré

peine à contenir

la fourrure épaisse

qui doit lui recouvrir le corps

&nbsp;

ces deux croûtes

juste sous ses clavicules

ce sont des branchies

qu'il a grattées jusqu'au sang

&nbsp;

impossible de voir ses yeux

jusqu'à ce qu'il ouvre sa visière

libère une flaque d'eau croupie

et quelques algues

qui lui pendent

comme une frange ridicule

du front jusqu'au larynx

&nbsp;

à en croire son regard

on dirait presque

qu'il se trouve encore

adapté à la situation

#lame

</div>

<div class="text">

il brandit devant lui

un bout de papier trempé

pas besoin de s'approcher

il n'y a qu'à voir son sourire

c'est un mandat d'arrestation

&nbsp;

il te demande

ta carte d'identité

et tu lui réponds que la marée

l'a emportée très loin

#messageinabottle

</div>

<div class="text">

même les menottes rouillées

qu'il passe à tes poignets

n'effacent pas ton sourire

&nbsp;

du haut de la tour

on te regarde monter

dans la fourgonnette

garée sur le trottoir

&nbsp;

de là où on est

on s'en veut de ne rien faire

on voudrait

sauter dans le vide

replier nos jambes

contre nos poitrines

et se laisser tomber

de tout notre poids

#biggersplash

</div>

<div class="text">

une dernière bombe

dans la piscine

juste éclabousser la gueule

des fonctionnaires cyniques

qui t'emmènent loin de nous

#boom

</div>

<div class="text">

alors on s'approche du bord

et on décide de le faire

&nbsp;

on saute

&nbsp;

on replie nos jambes

contre nos poitrines

mais quelques mètres avant

de nous écraser

sur le toit du fourgon

on déploie nos ailes

&nbsp;

toi tu lèves la tête vers nous

tu nous regardes planer

tu nous fais signe de la main

et tu sais qu'on se reverra

#cheesy

</div>

<div class="text">

alors c'est vrai

que c'est toujours pareil

&nbsp;

toute l'énergie qu'on met

pour faire tourner le vent

pour changer le sens

du chantier

pour poser quelques infrastructures

qui nous appartiennent vraiment

&nbsp;

toute l'énergie qu'on met

à maçonner des entailles

dans l'évidence

&nbsp;

tout ça pour finir

avec les yeux rouges

du gaz dans la gorge

et des barreaux à nos fenêtres

&nbsp;

ça donne envie de prendre l'air

&nbsp;

quand on voit

la douceur du carnage

on n'est pas sûr·es

que ça mérite

de devenir une société

&nbsp;

c'est un doute

dont on ne se séparera jamais

avec le reste de notre génération

&nbsp;

on n'est pas certain·es

d'avoir un avenir

parce qu'on n'est pas certain·es

d'être à la hauteur

suffisamment sérieux·ses

suffisamment empathiques

pour faire face

pour se sécher les épaules

étendre la nappe sur l'herbe

raviver les braises

mais pour incendier seulement

l'extrémité d'une clope

&nbsp;

attendre enfin en paix

la capitulation de la Terre

et l'extinction-soleil

#over

</div>
