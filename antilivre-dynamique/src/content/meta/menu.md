Il ne faut faire aucun effort pour flotter dans la piscine, et plus s’annonce l’extinction, plus on bronze.

Voyage, sois libre, pars à la rencontre de l’autre, découvre qui tu es vraiment.

Mais quand on est libre, quand on voyage, quand on s’accomplit soi-même, on devient du territoire plein.

Et tout autour de nous crève du manque d’espace.
