```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp00.jpg}
```
# 0. Incendie

les rues sont imbibées d'alcool

mais les caméras ont mouillé nos silex

même le néolithique a l'air moderne

plus personne ne sait faire du feu

à part les nuages

qui crament

#thisisfine

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp01.jpg}
```
# 1. Extérieur

là

on traîne sur le trottoir

en attendant le bus

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il fait froid

les arbres tirent un peu la gueule

on est encore mouillé·es

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}une pellicule de chlore sur la peau

les poumons dilatés que le vent laboure

tout l'iceberg amer

du corps qui vient de nager

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ce matin

les fleuves étaient fermés

alors on est allé·es à la piscine

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et plus on glandait

sur les dalles blafardes

dans nos maillots en polymère

plus on remarquait

combien les piscines municipales

ressemblent au capitalisme tardif

#swimmingwithsharks

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}1) il ne faut faire aucun effort

pour flotter dans la piscine

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}2) on a toustes appris à nager

dès l'enfance

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}3) il est déconseillé

de courir autour de l'eau

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}4) ce qui a de la valeur

reste cadenassé aux vestiaires

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}5) il n'y a pas beaucoup de place

dans les jacuzzis

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}6) le chlore ne nique pas les yeux

tant qu'on les garde fermés

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}7) il n'est pas autorisé

d'incendier le bâtiment

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}là où ce bus nous emmène

l'incendie prendra peut-être

en tout cas

on a enfilé nos k-way ignifuges

#wardrobessentials

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp02.jpg}
```
# 2. Intérieur

on débarque un peu sans prévenir

ça te dérange pas ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'était galère de les semer

on n'a pas vraiment eu le choix

on t'a perdu·e de vue

quand les flics nous ont dispersé·es

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}t'inquiète pas

personne ne nous a vu·es entrer

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}est-ce qu'on peut passer la soirée chez toi ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ça fait du bien de s'affaler

dans ton salon étrange

avec son désordre adolescent

son aquarium encrassé

son odeur

moitié kebab

moitié vernis à ongles

ses guirlandes lumineuses

que t'as volées chez Ikea

pour fabriquer un genre de cirque

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}au coin du canapé

des cadavres de bouteilles

des cadavres de bougies

même le cadavre d'une souris

qu'on croirait plongée

dans un sommeil réparateur

#creepycute

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}de ta fenêtre

on saisit la ville d'en haut

on réussirait presque

à rebâtir une distance

avec les néons

les trottoirs

les commissariats

au moins une distance intérieure

#skyscraper

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'odeur du guêpier

plane dans la métropole

mais perd de sa terreur

quand elle atteint le cinquième étage

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il est rassurant ton appartement

même si on sent bien

que les années sont passées

sans qu'aucune thune ne ruisselle

sur le cocon que t'as construit

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les anxiolytiques aussi

ont sculpté ta grotte

d'une croûte de chimie dure

qui s'épaissit avec le temps

c'est la texture amère

que prend la précarité

quand elle se pétrifie

#homedesign

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}t'es sûr·e que toi ça va ?

t'es arrivé·e y'a longtemps ?

t'as l'air encore un peu sous le choc

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}viens

on refait une de ces soirées

où on essaie de trouver encore

quelque chose à se raconter

à chaque fois

ça nous fait du bien

#healingjourney

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu le trouves pas étrange

ce concert de voix

qui répondent à l'urgence

en valorisant la complexité ?

en affectant la retenue ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}peut-être

que c'est nous qui vieillissons

nous qui ressentons

plus sensiblement qu'avant

combien pèse sur la nuque

l'injonction à la maturité

à la parcimonie

à la pacification

#wise

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu rigoles

mais est-ce que ce serait

vraiment si insensé

qu'à la sénilité des écosystèmes

en phase terminale

on réponde par la colère

évidente et incendiaire

de l'adolescence ?

#wiser

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp03.jpg}
```
# 3. Génération

t'as raison

tout se ressemble

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on s'est trompé·es tout à l'heure

on voulait entrer chez toi

mais on a débarqué chez les voisin·es

et on n'a pas su faire la différence

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'univers est misérable et homogène

homogène comme la misère

misérable parce que la marchandise

nous a rendu·es homogènes

#samesamebutdifferent

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors tout d'abord

nique les marchandises

ce qui a une valeur

c'est les histoires

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais ensuite

les histoires

on a du mal à les trouver

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu te souviens

cet après-midi

ce moment face à la foule

t'avais le mégaphone dans les mains

et soudain

plus rien à dire

#findyourpurpose

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}t'y es pas pour grand-chose

en tant que génération

on n'arrive pas à se raconter

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}faut qu'on fasse le deuil

de toute parole

absolument commune

faut qu'on lutte chaque jour

contre l'idéal uniforme

d'une paire de cordes vocales partagée

qui ferait vibrer la gorge

de toute la génération

#thevoice

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on dit pas ça pour rappeler

que la diversité des opinions

c'est super chouette

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}depuis à peu près deux cents ans

les dominants répètent en boucle

qu'il faut des points de vue divergents

pour garantir la pluralité

du débat démocratique

#makeup

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et

depuis à peu près deux cents ans

les dominants continuent de dominer

alors bon

on va y aller mollo

avec la pluralité

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}se couper la tête

dans des débats sans fin

c'est devenu une valeur

c'est devenu un jeu

c'est devenu l'équilibre absurde

comme condition de l'agression

toujours reconduite

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}conclusion locale de l'argumentaire :

la génération démocratisée s'autoguillotine

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais rappel du premier argument :

rêver la monophonie

c'est se crever les yeux

avec le cutter de l'idéalisme

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors déduction semi-générale :

on sait pas vivre les yeux ouverts

sans se couper la tête

#hangover

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et là

on amène la conclusion définitive

qui sera notre théorie principale :

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}partout

c'est la grosse piscine

et tout le monde nage

#poolparty

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la parole

qu'on va tenter ensemble

en plein cœur du paradoxe

c'est celle des enfants

de la classe moyenne défaitiste

avec leurs sourires ubérisés

leurs empathies qui défaillent

et leurs suffisances occidentales

#pov

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'histoire qu'on va un peu réécrire

et un peu inventer

c'est celle de la génération extinction

c'est celle de la génération piscine

et à la fin on va rêver un peu

en laissant la parole

à la génération premier degré

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est comme ça qu'on voudrait vivre

au premier degré

t'as déjà vu quelqu'un

pleurer au second degré ?

#nofilter

#forreal

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp04.jpg}
```
# 4. Avenir

t'es pas certain·e d'avoir un avenir

c'est une angoisse que tu partages

avec le reste de notre génération

on n'est pas certain·es d'avoir un avenir

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on n'est pas certain·es

que des livres d'histoire

parleront un jour de nous

parce qu'on n'est pas certain·es

qu'il reste encore assez d'avenir

pour écrire des livres d'histoire

#soldout

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le problème

c'est que ce monde est tout neuf

alors qu'on a encore

le visage de nos parents

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}chaque matin

on revit

la vieille sensation du cycle

on ouvre les yeux

on sent le soleil

sur nos visages

tout est radieux

tout est à faire

#trusttheprocess

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}surtout l'été

surtout dans les villes

surtout quand t'as des thunes

surtout quand ta peau

est blanche comme l'insouciance

#blessed

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}rien

à aucune intersection

d'aucun des cercles

rien

n'a l'air condamné

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout pulse

une énergie

impossible à épuiser

et tout court à l'épuisement

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout court à l'épuisement

parce qu'on existe

c'est le principe actif

d'une angoisse très particulière

la certitude

que sa propre présence est dévastatrice

dans un univers où tout sonne

comme une injonction

à exister plus fort

#monsterenergy

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}voyage

sois libre

pars à la rencontre de l'autre

découvre qui tu es vraiment

choisis ce plan d'épargne avantageux

pour les moins de vingt-cinq ans

ne doute jamais

de la nécessité

de ta présence

#youonlyliveonce

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais quand on est libre

quand on voyage

quand on s'accomplit soi-même

on devient du territoire plein

et tout autour de nous

crève du manque d'espace

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors chaque été

après une bonne année de taf

on remplit les mêmes plages

pour essayer de faire le vide

#letitgo

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}toi tu fais comment

quand t'arrives plus

à faire le vide ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est sûr

faudrait qu'on réussisse à imaginer

autre chose que la catastrophe

mais notre génération

n'a plus aucune confiance

en son imagination

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la preuve

on n'a plus beaucoup d'ami·es

on n'a même plus d'ami·es imaginaires

#fakenews

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}interrogé·es par un institut de sondage

60 % des 18-30 ans déclarent

« ne pas avoir de vrai·es ami·es »

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}à la question

« comment envisagez-vous l'avenir ? »

83 % ont répondu

« on va devoir se débrouiller seul·es »

#lucidity

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp05.jpg}
```
# 5. Neige

chez toi

le monde est calme

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}juste en glandant sur les coussins

ou en errant sur instagram

on atteint par le creux

une simulation

de paix intérieure

#truth

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais on se demande quand même

pourquoi t'as claqué ton salaire

dans ce tapis de course ridicule

#innergrowth

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on a ramené des chips

elles sont dans le sac à dos noir

qui traîne près de la table

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu voudrais pas aller chercher

quelque chose à boire ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}depuis la cuisine

un air glacial

s'engouffre dans le salon

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est la porte du frigo

qui s'est cassée

quand t'as essayé de l'ouvrir

#broke

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}du frigo béant

le blizzard souffle si fort

que le froid commence

à nous mordre

sous les ongles

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}doucement

nos mains bleuissent

leur chair se fige

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il faudrait allumer la hotte

pour qu'elle aspire

les flocons qui volent

au milieu des poêles

#winterwonderland

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le givre se condense

et dessine des glyphes

sur le carrelage de la cuisine

#abstract

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on croirait presque entendre

la respiration du gel

on croirait presque

qu'une énergie autonome

anime son expansion

#hauntedhouse

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il est lent

il recouvre les murs

il atteint la fenêtre

il opacifie le soleil

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quelques minutes à peine

et une neige épaisse

tapisse le parquet

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}regarde comme les gerçures

ont vallonné nos paumes

regarde nos métacarpes à nu

nos empreintes digitales

illisibles

#nailporn

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}des stalactites de glace

fixées au plafond

perlent quelques larmes

sur le simili-cuir du canapé

où nous nous blottissons

comme une portée si frêle

que l'hiver lui crevassera les os

le cœur

et tout le désordre intérieur

#petsofinstagram

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le sang du rongeur mort

gèle brutalement

et dans les ténèbres

du couloir

on entend quelque chose rugir

#primalfear

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}pourquoi tu craques

toutes ces allumettes ?

pourquoi tu les avales

quand elles s'apprêtent à mourir ?

#youarewhatyoueat

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il n'y a plus rien à faire

juste rester prostré·es

sous les plaids premier prix

et se ronger

les doigts

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ou s'en servir

de ses doigts

pour dessiner des formes

sur le cellophane de buée

qui recouvre

toutes les vitres du salon

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est marrant

à travers les lettres

que tu traces

on devine la ville

qui bruit dehors

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}t'as vu comme il fait sombre ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est pas normal

on est en plein été

on est en plein jour

mais on va s'y habituer

tu verras

on s'habitue à tout

#winteriscoming

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp06-date-1.jpg}
```
# 6. Glaciation

```{=tex}
\begin{datation}

décembre 536

276.67 ppm de CO\textsubscript{2}

\vspace*{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}si tu colles bien ton nez

au verre de la paroi

tu verras les reflets

devenir des formes

les immeubles au loin

qui commencent à danser

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}puis s'effacent

sous un ciel cendre

carrément noir certains jours

même si la lueur du soleil

ne disparaît jamais complètement

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on n'est plus au 21^e^ siècle

on est en 536

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la tectonique est déchaînée

les volcans des deux hémisphères

vomissent pendant plusieurs mois

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}une poussière lourde

cimente le ciel

l'atmosphère devient si opaque

que le soleil ne passe plus

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la planète entière

recommence sa glaciation

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}au coin des rues

tapissées de paille

de grêle violente

et d'animaux décagés

tu les entends partout

les prophètes et les serfs

qui annoncent la clôture

du règne humain

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}partout on murmure

que la lumière s'est éteinte

à jamais

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}dans ce nouveau monde

la nuit est la règle

et tu dois réapprendre

la mécanique des plantes

réapprendre à te nourrir

en sachant cultiver

ce qui pousse sans soleil

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}dans le pétrole des villes

dans les nuits absolues

qui couvent les campagnes

contrairement à ce que t'aurais pu croire

l'obscurité est accueillante

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les rugissements dans le noir

ont un air presque doux

la terreur primale

disparaît peu à peu

et tu t'habitues

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu verras

on s'habitue à tout

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la poussière volcanique

abolit les saisons

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}cette année-là

il neige

en plein mois d'août

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la température des rivières

augmente brutalement

tu t'y baignes avec plaisir

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}certaines espèces de poissons

disparaissent

d'autres s'adaptent

deviennent translucides

et froides

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}durant les premiers mois de glaciation

plusieurs émeutes

agitent la planète

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les palais se vident

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}face à l'extinction

on ne comprend plus bien

l'intérêt qu'on trouvait autrefois

à obéir

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout le monde

a emménagé dans la nuit

alors plus personne

ne paie d'impôts

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}partout sur les lèvres

du vieil ordre politique

juste des gerçures

et la sécheresse du silence

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}juste des peaux mortes

qui se détachent

avec lenteur

et fertilisent un monde

que tu foules

sereinement

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}au Japon

durant le printemps 536

un édit impérial

décrète que la nourriture

vaut plus que les perles

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}en Scandinavie

les gouvernants paniqués

incendient les réserves d'or

pour apaiser leurs dieux

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}à Byzance

pendant l'hiver de juillet

la noblesse échange ses demeures

contre des charrettes

et part sur les routes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}partout sur les lèvres

du vieil ordre de la valeur

le sel de la mutation

érode les chairs

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}si tu glisses un œil

derrière les remparts à l'abandon

tu verras que les palais

se remplissent à nouveau

d'un monde éclaté

végétal et curieux

qui reprend possession

des salons

des chambres

et des écuries

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais le temps passe

et avec une pointe de tristesse

tu constates que la lumière

perce à nouveau les nuages

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le soleil revient

et avec lui les polices

comme toujours

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}elles vident par la force

les palais occupés

sans pouvoir effacer

qu'autre chose

un bref instant

y a trouvé une forme

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les vents finissent

par disperser la cendre

par faire oublier

que les dérèglements de température

ont bien failli abolir l'autorité

et congédier la valeur

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la nuit accueillante se dissipe

le soleil trône à nouveau

et les empires s'en sortent

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}pour le moment

```{=tex}
\end{datation}
```
```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp07.jpg}
```
# 7. Dégel

odeur de musc

confort animal

on voudrait encore

rester blotti·es contre toi

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}pour le moment

on coagule

en un ventre unique

on s'agglomère

autour d'un rêve commun

#comfy

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ce rêve

c'est la rupture de l'hibernation

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}comme le soleil

lèche nos visages

une moiteur nouvelle

mouille nos organes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les rayons gamma rebondissent

sur le parquet couvert de neige

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}fais attention à ton visage

couvre-le de tes mains

ne laisse pas tes yeux brûler

#enlightenment

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout brille

tout est aveuglant

il est devenu difficile

de faire la différence

entre la totalité du réel

et une barre de néon

#retrofuture

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}restons infrarouges

c'est trop tôt encore

pour parvenir à penser

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les tuyaux des radiateurs

explosés par le gel

pissent dans l'appartement

des jets à haute pression

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il faut tenter nos premiers pas

à travers les geysers

tituber ensemble

au moins jusqu'à la cuisine

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}maintenant

la cafetière est sur le feu

la température monte

#countdown

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et on regarde le café brûler

comme on regarde avec indifférence

brûler les pays

où poussent les caféiers

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est tout l'ordinaire

un malaise après l'autre

qui se reconstruit

#coffeelovers

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il n'est pas si tard

l'hibernation aura été

curieusement brève

on a l'impression

d'avoir laissé fondre

des années entières

#timelapse

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}attention

en réchauffant tes mains

n'approche pas ton pelage des flammes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu ne l'avais pas vu

ton pelage

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}chaque centimètre de nos peaux

s'est couvert d'une fourrure épaisse

c'est pour ça qu'on avait chaud

nos organismes réagissent à l'hiver

#furryfriends

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on devra prendre soin

de ce manteau confortable

et vu comme il se densifie

il faudra le brosser souvent

#curlyroutine

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu voudrais pas

nous couper les cils ?

ils sont beaucoup trop longs

on pourrait presque

se les ranger derrière l'oreille

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}regarde-toi dans la fenêtre

t'as l'air d'un animal

mais ça te va bien

t'es même assez photogénique

avec tes lunettes de soleil

debout à contempler la ville

sans quitter ton sourire

de grand mammifère triste

#beautyandthebeast

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les radiateurs crevés

continuent de s'épandre

le niveau de l'eau monte

la température grimpe aussi

#countdown

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors on casse

la pointe d'une stalactite

et on la regarde

flotter dans le café noir

un dernier frappuccino

avant l'extinction

#baristalife

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp08.jpg}
```
# 8. Autorité

le frappuccino à la main

tu as pris l'habitude d'être libre

c'est une routine que tu partages

avec le reste de notre génération

on a pris l'habitude d'être libres

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et quand on sait

que l'habitude

est une forme douce et inconsciente

de captivité

c'est là

sans doute

qu'il aurait fallu s'inquiéter

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le pouvoir vertical

qu'ont connu nos parents

a creusé le bassin

et installé les chaises longues

où on profite du soleil

#itsatrap

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}maintenant le bassin est rempli

et le pouvoir vertical

entre dans la phase programmée

de son obsolescence

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}aujourd'hui

on se marre pas mal

quand on entend

des discours trop écrits

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quand on voit les treillis militaires

les hélicoptères ridicules

les robes colorées des magistrats

toute cette ruine d'autorité agonique

qui cherche à rester un spectacle

pour retarder

sa propre disparition

#retromovie

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et quand on rit

de ces ruines d'autorité

tout en lignes

en angles droits

en frontispices

quand on se fout de leur gueule

on ne voit pas qu'on nage

une brasse coulée et triste

#trapped

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}aujourd'hui

le pouvoir est devenu liquide

et on nous a inventé

plein de nouvelles natations

#neverendingstory

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors plus personne

ne sait faire du feu

parce que tout le monde

boit la tasse

à part le soleil

qui incendie les nuages

#cheers

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp09.jpg}
```
# 9. Soleil

ta transpiration est agressive

elle nous stresse

arrête de courir

sur ce tapis roulant

là tu pues le vieux fauve

qui a trop chassé dehors

et ça colle pas du tout

avec ton sourire idiot

de gosse enfermé·e

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu t'es toujours pas réchauffé·e ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}certains jours on se demande

où tu trouves ces calories

que tu brûles en ligne droite

sans jamais avancer

#cardio

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}certains jours on sait plus

ce qu'il faut affronter

mais on reste stoïques

et on se bat contre tout

#eyeofthetiger

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}feu dans toutes les directions

feu sur soi

et feu sur la planète entière

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est qu'on a déclaré la guerre

au soleil

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est une course

à qui dévorera la planète

en premier

#competitiveeating

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les astronomes

les plus optimistes

estiment que le soleil

consumera notre monde

dans sept milliards d'années

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors

en tant qu'espèce

on accélère

pour y arriver avant

#eventplanner

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on est l'outsider

sur un côté du ring

contre la montre

tête dans le guidon

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le soleil c'est la force tranquille

il attend que la gravité

amène la terre dans ses bras

pour brûler infiniment

#love

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on est légion

on peut cramer la Terre

le soleil est colossal

il doit attendre

et douter

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors c'est vrai

qu'on pourrait s'accorder une pause

regarder l'extinction-soleil

comme une promesse lointaine

vivre avec la mort cosmique

comme horizon tolérable

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais on ne sera plus jamais

capables de faire une pause

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}longtemps

on s'est couché·es de bonne heure

mais plus maintenant

maintenant on ne dort plus

#spacecake

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il y a toujours une bonne raison

de rester éveillé·es

un truc à regarder

un truc qui vient remplir

le tonneau percé

de nos récepteurs dopaminergiques

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le pouvoir liquide

nous a rendu·es accros à l'addiction

accros à la quasi-noyade

comme horizon nécessaire

#sunrise

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le pouvoir liquide

nous a filé des brassards

juste assez gonflés

pour oublier

l'horizon tolérable

de l'extinction-soleil

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le pouvoir liquide

organise sans se presser

notre extinction-piscine

#sunset

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp10.jpg}
```
# 10. Inondation

débranche le tapis de course

le salon continue de se remplir

l'eau nous arrive aux genoux

et on ne va pas risquer le court-circuit

#interiorlighting

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu sens

comme le sel brûle les lèvres ?

c'est l'iode de nos sueurs

et c'est celle de l'océan

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les canalisations explosées

inondent l'appartement

le niveau de l'eau monte

#countdown

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on n'aura pas le temps

de plastiquer les fontaines

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quitte à finir trempé·es

c'est l'occasion

d'inventer quelque chose

#betterworld

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on pourrait se rouler un joint

mais il nous faudrait un siège

un peu plus imperméable

que cette épave de canapé

pour le fumer tranquillement

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il y a encore

les matelas gonflables

qu'on avait pris ce matin

en allant à la piscine

parce que les fleuves

étaient fermés

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on pourrait aussi

commander des sushis

essayer comme on peut

d'habiter l'inondation

#scubadiving

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}voilà

là on est bien

on plane sur les matelas

on se laisse rebondir

entre les quatre murs

de ton appartement

on pratique la navigation d'intérieur

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et plus la weed

nous bronze le cerveau

plus on rêvasse en crawlant

de la cuisine au salon

et plus la ligne de flottaison grimpe

plus l'appartement

remonte à la surface

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le canapé

les plantes artificielles

les bouteilles de shampoing

tout un étrange continent postindustriel

qui dérive en pirate

sans jamais aller bien loin

#wander

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}regarde

si on se penche un peu

on voit de gros poissons marins

qui nagent

l'air intrigué

autour de ton aquarium

et à travers la fenêtre

qui donnait sur la ville

d'autres poissons

nous regardent regarder

toujours ton aquarium

#metavers

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on sonne à la porte

les sushis viennent d'arriver

et ce serait cool de manger

devant quelque chose

#foodgasm

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}heureusement

la télé flotte

comme un genre de bouée

arrimée au sol

par son câble d'alimentation

elle diffuse en boucle

un documentaire animalier

#wildlifelovers

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'eau va bientôt

atteindre le plafond

et nous écraser contre lui

#dontlookup

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}touche tes clavicules

juste sous ton pelage

tu sens cette fente inhabituelle ?

ce truc spongieux

et curieusement frais ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}maintenant

on a des branchies

#mermaidsarereal

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on peut immerger nos visages

inspirer sans entrave

nos corps filtrent l'oxygène

ils relâchent des chapelets de bulles
```{=tex}
\vspace{1\baselineskip}
```

on est souples

fluides malgré la fourrure

prêt·es à explorer

le chaos humide

des fonds marins

#underwaterselfie

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}laisse tes branchies

se gonfler

se vider

comme des siphons

et recommence

recommence encore

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}nos poitrines respirent

avec l'obstination

de ces poissons migrateurs

qui survivent aux typhons

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}une pieuvre curieuse

s'approche de la télé

ces eaux sont trop froides

pour ses tentacules fragiles

qui préfèrent se lover

contre la chaleur de l'écran

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le rythme des images

change la couleur

de sa peau

même la texture

de son corps

#glitchart

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}cette odeur de décomposition

qui filtre à travers

la puanteur des algues

c'est le cadavre du rongeur

qui dérive avec nous

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il faut qu'on évite

ses tripes humides

ses entrailles mouillées

où habitent les champignons

les bactéries

#viral

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}de là où on est

on ne voit qu'un corps bouffi

mais dans le secret

de la forgerie moléculaire

le bacille entame sa lutte

elle améliore son logiciel

se divise en séquences

devient une machine

à produire des versions

#update

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la salive

lui monte aux lèvres

le dégel

lui rend l'appétit

elle dévore les rayons du soleil

nourrit son empire

de chairs gonflées

jusqu'à installer ses colonies

dans nos cœurs

dans les trois cœurs de la pieuvre

et dans ceux des insectes

#stayhome

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp11-date-2.jpg}
```
# 11. Peste

```{=tex}
\begin{datation}

novembre 1340

281.07 ppm de CO\textsubscript{2}

\vspace*{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et les insectes infectés

courent sur les rats

qui courent sur les pavés

du jeune 14^e^ siècle

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}nos matelas ont dérivé

jusque dans les eaux territoriales

du port de Catane

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}de là où on est

l'agitation du vivant est perceptible

à la quantité de marchandises

qu'il empile

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'échange réinvente sans cesse

de nouvelles manières

de se rendre meurtrier

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}cette année-là

les flottes marchandes de Gênes

importent en Sicile

quelques rats malades

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on n'imagine pas encore

que la peste noire

deviendra « la mort noire »

et tuera

près d'un tiers de l'Europe

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors que les premiers bubons

explosent sur des cadavres italiens

la nouvelle de l'infection se propage

comme une épidémie

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et bientôt l'infection

devient pandémie

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout tremble autour de nous

le siècle a la peau qui frissonne

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les royaumes réagissent

ils mettent en place

des procédures de quarantaine

bouclent les ports

massacrent les rats

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les guérisseurs et les guérisseuses

redoublent de ferveur

pour soigner les maîtres et les maîtresses

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}depuis leurs territoires glacés

les gouvernances nordiques

croient avoir le temps

elles se préparent

à protéger leurs puissants

de la peste continentale

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}en Norvège

de nouvelles lois sont édictées

elles interdisent aux paysans et aux paysannes

d'entrer dans les enceintes protégées

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on triple le nombre de médecins

affectés au roi et à sa cour

on sécurise les ports

on entame la construction

d'immenses murailles

autour des maisons les plus riches

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quand les années 1340 finissent

la peste touche l'Angleterre

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quelques mois plus tard

un bateau quitte le port de Londres

en direction du Danemark

avec à son bord

une cargaison de laine

un médecin

un dispositif de quarantaine

et un couple de rats

caché dans la double coque

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}pendant quelques jours

les rats et les marins

habitent en harmonie

se nourrissent mutuellement

leurs environnements parasitaires

entrent en symbiose

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on observe les symptômes

sur un premier marin

qu'on met en quarantaine dans la soute

puis un deuxième

un troisième

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}très vite

le bateau n'est plus habité

que par deux rats solitaires

qui ignorent tout du pilotage

mais disposent

d'un immense garde-manger

et ils commencent à se reproduire

pour tuer le temps

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le navire dérive sur la mer du Nord

pendant plusieurs semaines

il échoue finalement sur la côte

non loin du port de Bergen

au sud de la Norvège

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}adossé·es à un rocher

quelque part sur la plage

on regarde les jeunes rats

fouler la terre ferme

pour la première fois

de leur courte vie

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}leurs parents

ont connu cette sensation

avant le déluge

et ils les regardent

tendrement

s'éloigner vers la ville

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les noblesses nordiques

ne sont pas prêtes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les rats se moquent

de leurs remparts

la classe possédante

subit de plein fouet la pandémie

80 % de l'élite meurt

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les historien·nes les plus optimistes

estiment que la peste noire

a décimé

un quart de la population norvégienne

certain·es disent un tiers

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}nombre de terres

campagnes

bras de mer

plateaux montagneux

sont laissés sans gouvernance

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors la gouvernance

s'organise d'elle-même

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}sous le coup de la nécessité

certaines communautés

abolissent le principe de propriété

et le territoire se recompose

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on crée des communes

on édicte des lois libertaires

on établit le droit coutumier

de l'allemannsretten

ou "liberté d'errer"

encore en vigueur de nos jours

stipulant que le droit des humains

à bénéficier de la nature

prévaut sur la propriété privée

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}parmi les nobles qui survivent

beaucoup doivent renoncer

à dominer

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'extinction du peuple

ne permet plus

de lever un impôt suffisant

pour leur épargner le travail

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}partout dans le pays

les nobles retournent aux champs

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais le temps passe

et tu remarques les bubons

qui s'effacent de tes bras

la peau sous ton pelage

qui retrouve la santé

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la bactérie s'éloigne

avec l'air moqueur

d'un volcan qui s'éteint

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il suffira de quelques années

pour que le vivant

relance le cycle

de l'accumulation

et de la rareté organisée

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quelques années à peine

avant que les nobles

ne retrouvent leurs donjons

à l'abri des pandémies

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}pour le moment

```{=tex}
\end{datation}
```
```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp12.jpg}
```
# 12. Marée basse

la télé déconne

encore une latte de joint

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}litre après litre

l'appartement inondé se vide

et on a un dîner à finir

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est notre tour de manger

laisse la barquette

voguer sur l'eau

elle finira par croiser

la trajectoire de nos flotteurs

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}merde

elle dérive vers le couloir

et on est trop défoncé·es

pour rattraper les sushis au saumon

qui remontent le courant

#mainstream

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}plus le niveau de l'eau baisse

plus on discerne les murs

violemment lézardés

tout le papier peint qui pèle

le monde après le déluge

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les dangers du grand large

sont derrière nous

on va bientôt pouvoir

rouvrir la fenêtre

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}fais attention

ton matelas se dégonfle

il s'est empalé sur les oursins

c'est ça qu'on entend siffler

en retrouvant la terre ferme

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}nous aussi

on touche le fond

on retrouve un peu

le sens de la gravité

#lifestyle

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}à sécher nos pelages

en nous secouant comme ça

on a l'air de chiens mouillés

#wetfurryfriends

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la marée a emporté l'aquarium

les plantes en plastique

et ta carte d'identité

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}elle a laissé derrière elle

une étendue grouillante

où nos matelas traînent

comme des épaves

fluos et ridicules

un peu anachroniques

au milieu des ordures

des mollusques égarés

des bouteilles sans message

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}t'as vu cette canette de coca ?

elle bouge toute seule

sur ton tapis roulant

un bernard-l'hermite

l'a prise pour coquille

#tastethefeeling

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est le truc le plus drôle

qu'on ait vu de la journée

mais ça laisse

un grand vide à l'intérieur de l'estomac

faut penser à autre chose

```{=tex}
\vspace{1\baselineskip}
```

l'océan a laissé le salon

en chantier

mais il a l'air plus fertile

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le roulis des vagues

a sculpté une caverne

curieusement habitable

#beforehistory

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu te souviens

du jour où t'as réalisé

que l'humain était capable

de rendre un bout du territoire

parfaitement inhabitable ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}nous on s'en souvient

on était enfants

c'était pas vraiment un choc

on l'a métabolisé

comme une évidence

nucléaire

qu'on n'a plus jamais questionnée

#selfbelief

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}viens t'asseoir avec nous

mais regarde où tu mets les pieds

le plancher capitule

sous la pression des coraux

un jeune couple de crabes

trimballe même son exosquelette

sur ce qu'il reste

de ton tapis

#workout

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les larmes te montent aux yeux ?

alors faut fumer

une dernière latte

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}pleure pas comme ça

regarde les crabes

ils cahotent parmi les ruines

ils nagent dans le bonheur

#urbex

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp13.jpg}
```
# 13. Dépression

la télé déconne toujours

et ce trou dans ton ventre

plein à craquer

de poisson cru

c'est pas seulement la weed

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}même quand on n'est pas défoncé·es

la vidéo et la réalité

sont devenues un espace

homogène

à l'intérieur duquel on teste

des scénarios prédéterminés

#boomerang

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est vrai

qu'on vit encore assez bien

la réduction croissante

de nos palettes émotionnelles

#goodvibesonly

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais quand on en arrive au stade

où tout le film nous indiffère

on sait plus

si on est solides émotionnellement

ou si on est juste devenu·es

des figurines interchangeables

qui tapent des siestes à onze heures

#beautysleep

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est assez inédit

comme forme de tristesse

l'extinction de l'espèce

la dissipation du réel

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}solastalgie

ou éco-anxiété

même les psychiatres

ont inventé des mots

pour dire la position foireuse

où on a rangé la psyché

de toute une génération

qui compense le spectacle

de l'autodestruction

par un afflux excessif

de stress prétraumatique

#mindset

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}si tu nous regardes bien

certains soirs

tu verras juste d'habiles junkies

qui savent se frayer

un chemin cynique

dans la banqueroute générale

de sérotonine

#traderlife

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors quand on se défonce

pour arrêter d'être tristes

et qu'on finit

tristes et défoncé·es

on comprend

que la dépression hivernale

survivra au réchauffement climatique

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on comprend

qu'on traînera encore longtemps

dans nos villes de smog

où le concept de non-fumeur

est devenu une abstraction rassurante

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}attaché·es au mât

malgré le naufrage

la tête juste assez émergée

pour écouter d'autres sirènes

que celles des keufs

#oceansounds

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la tête juste assez immergée

pour entendre

les milliers d'injonctions

à devenir soi-même

dans les strictes limites

de la zone industrielle

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les générations avant la nôtre

ont décrété

que la métaphysique était vide

et qu'il fallait se battre

pour le réel

#okboomer

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}notre génération a décrété

que le réel était vide

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors on s'est dépêché·es

de passer des diplômes en communication

pour organiser la mise en langage

du précipice insoutenable

#hashtag

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}pour oublier que la différence

entre un bureau et une barricade

c'est 90 degrés d'inclinaison

#doityourself

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp14.jpg}
```
# 14. Pâtures

la décomposition des algues

fige un genre de prairie

qui repousse

en touffes herbeuses

jusqu'à recouvrir le plancher

#cottagecore

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la plage

prend des allures bucoliques

et l'eau condense

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il nous faudrait un linge sec

quelque chose

qui nous couvre le visage

ou au moins les yeux

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la fumée est lourde

l'humidité étouffante

elle irrite les branchies

qui nous collent aux clavicules

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on dirait un peu

le brouillard lacrymogène

qu'on s'est mangé tout à l'heure

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ça y est

on te voit plus du tout

tout est englouti

tout est opaque

#gloomyday

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu nous entends encore ?

essaie de suivre

le son de nos voix

tu sens cette énergie étrange

qui grésille dans ton cou ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ouvre la bouche

laisse les ultrasons

s'échapper de ta gorge

ils rebondissent sur les murs

ils cartographient l'espace

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'écholocalisation

nous aide à habiter la brume

l'air autour de nous

comme une fine toile tressée

fait vibrer nos Wi-Fi intérieures

ton appartement est minuscule

mais avec les ricochets hertziens

qui nous jaillissent de la bouche

il a l'air immense

#palace

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'intensité des ondes

détaille dans nos cerveaux

les masses

les volumes

et les distances

en d'étranges informations sensibles

qu'on peine encore à décoder

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on perçoit presque

les fluides qui sursautent

dans les tuyaux de l'immeuble

et les corps de tes voisin·nes

qui doivent muter aussi

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on localise même

une nappe huileuse

cent mètres sous terre

peut-être du pétrole

une fortune endormie

sous les parkings de la ville

#makeitrain

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}t'entends ?

quelque chose bêle dans le couloir

quelque chose émet de l'amour

quelque chose signale sa position

au reste du troupeau

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quand tu nous parles

en émettant des ondes

ta voix a l'air autotunée

t'es devenu·e une star

rupestre et radioactive

piégée dans une machine à fumée

avec les deux pieds dans l'herbe

#greenthumb

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on pourrait presque découper

de gros cubes de tourbe

et allumer un feu

au milieu du salon

on sera mieux au chaud

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}regarde nos reflets

dans la fenêtre couverte de mousse

moitié ovipares

moitié locataires endetté·es

moitié fauves

moitié déchets nucléaires

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et puis ouvre-la

la fenêtre

tu sens ce vent chaud ?

il annonce l'orage

alors laisse ton reflet

s'en aller librement

#xoxo

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp15-date-3.jpg}
```
# 15. Éruption

```{=tex}
\begin{datation}

mai 1789

278.57 ppm de CO\textsubscript{2}

\vspace*{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}arrête de fixer bêtement l'horizon

et regarde

où tu mets les pieds

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les cultures que tu foules

abritent une vie entière

des insectes étranges

qui commencent à transpirer

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}au nord du continent

les volcans s'éveillent

à nouveau

leur cendre refroidit

et fige la stratosphère

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}avec un sourire de vacarme

les nuages venus du nord

s'apprêtent à ouvrir

une parenthèse politique

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ils entament le grand dérèglement

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'hiver 1787 est trop doux

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}sans la croûte de gel meurtrière

les nuées de parasites subsistent

s'aventurent vers les pousses

laissent fleurir les mauvaises herbes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'œil des paysan·nes de France

se teinte d'inquiétude

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le printemps s'acharne

plusieurs coups de chaleur

détruisent les blés

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les récoltes précoces sont trop faibles

et des millions d'enfants sans bec

raclent la terre

picorent les insectes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on attelle quand même les animaux

quelques charrettes

pour monter à la capitale

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quand la campagne

commence à brûler

c'est toujours une bonne idée

de monter à la capitale

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}sur la route

observe comme tout le territoire

est strié de champs

de forêts domestiques bien alignées

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout le territoire d'Ancien Régime

suit sa propre géométrie

hyperlisible pour les autorités

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}de sa fenêtre

le regard du roi

embrasse un monde

conçu pour lui

de sa fenêtre

il constitue

la visibilité de ses sujets

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais à l'été 1788

la fenêtre du roi explose

et avec elle

toutes les vitres du palais

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les tableaux se trouent

les membres amputés des statues

tombent en silence dans les jardins

comme les jarrets mal anesthésiés

des animaux à l'abattoir

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ce n'est pas encore le peuple

c'est un orage de grêle

qui dévaste le pays

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}regarde où tu mets les pieds

ne marche pas en chaussettes

les 11 749 verrières de Rambouillet

sont en morceaux

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}après l'orage de grêle

le prix du pain monte

les récoltes de l'année suivante

sont pires encore

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}pourtant

dans sa lucidité proverbiale

de vieux riche idiot

louis XVI autorise

l'export du blé

et les possédant·es

les plus riches

s'en donnent à cœur joie

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}à la fin du printemps 1789

les estomacs vides

de blé mort ou vendu

se sont emplis d'insoumission

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout le territoire d'Ancien Régime

est une manifestation du corps royal

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}derrière la douceur des champs

on voit le gant de dieu

et dans le gant

la main du monarque

intransigeante

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quand les récoltes sont bonnes

c'est le roi qu'on acclame

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}depuis des siècles

le cycle qui fait la vie

et la subsistance commune

dispose d'un garant royal

signifie une autorité rassurante

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout se trouble

puis tout s'effondre

l'orage dans sa grande liberté

pompe ses watts

dans la faim

devient foudre insurrectionnelle

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il signe la mort de la sûreté

la nécessité de l'inconnu

l'imprévision

engagée de force

jusqu'à déchirer le cycle

de la répétition politique

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}même les réchauffements

même les dérèglements

les plus attendus

conservent cette étrange aptitude

à susciter la surprise

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}à la fin de l'été 1789

Paris brûle

et le sang bleu des nobles

réchauffe enfin la terre

qui cicatrise

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}pour le moment

```{=tex}
\end{datation}
```
```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp16.jpg}
```
# 16. Cyclone

on est bientôt

au bout de nos peines

le vent commence

à disperser la brume

mais l'orage plane

#cloudscape

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la fenêtre ne ferme plus

on l'entend claquer

presque aussi fort que nos dents

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les papillons s'évaporent

les ailes des mouettes

battent sans avancer

le bernard-l'hermite

dans sa canette de coca

s'est envolé comme une feuille

#lonelyboy

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les murs commencent à grincer

sous la pression du vent

l'horizon se brouille

un morceau du plafond se décroche

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ça y est

on est au cœur de la tempête

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}comment tu fais

pour rester stoïque ?

toujours barricadé·e

impossible à atteindre

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mets-toi à l'abri

l'appartement tout entier valse

ce qui était vissé cède

ce qui était collé tombe

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les casseroles s'envolent

de la cuisine au salon

et le contenu des étagères

se fracasse sur le plancher

les jeunes plantes

qui s'installaient

entre les lattes

sont déracinées

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu marches jusqu'à la porte

tu espères t'enfuir

mais en l'ouvrant

tu ouvres à la tempête

qui te repousse violemment

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}un premier mur grince

sous la pression de l'air

le crépi se décolle

et embarque avec lui

toute l'isolation

tout le ciment que le vent écartèle

comme un agrume dérisoire

#rollercoaster

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}un autre mur est emporté

éventré plutôt

mais le squelette des parpaings

survit au choc

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}rejoins-nous sous le canapé

et ferme les yeux

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ne rien voir

c'est encore le plus sûr

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}attendons que ça passe

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}voilà

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on dirait que le vent se calme

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on dirait qu'il relâche son étreinte

pour tourner autour du bâtiment

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu peux rouvrir les yeux

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}lève la tête

il n'y a même plus de toit

tout l'espace est démuré

toute la ville peut nous voir

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'œil du cyclone

nous fixe sans cligner

#dontblink

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}restons à l'abri sous le canapé

bien au centre de la pièce

le tourbillon aspire nos corps

vers les bords de la plateforme

et plus aucune barrière

ne nous sépare du vide

#mindthegap

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on s'écraserait

cinq étages plus bas

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}assieds-toi

ne marche pas

si près du gouffre

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}passe une main dans ton dos

tu sens

ces deux moignons infantiles ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}du sang

des plumes

qui percent la fourrure

juste là

entre tes omoplates

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tes ailes ne sont pas prêtes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}s'il te plaît

reviens vers nous

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il nous faudrait un bout de tôle

du plexiglas opaque

du carton

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}n'importe quoi

tant qu'on peut s'y cacher

construire une forteresse

même éphémère

rester imperceptibles

éviter de s'exposer

pas trop se débattre

#peacefulwarrior

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on finit trop souvent

par se débattre

à l'intérieur des murs

qu'on a abattus

#defeatedwarrior

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le canapé ne suffit pas

va falloir renverser l'appartement

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}empiler les étagères

refixer les trucs les uns aux autres

la porte du frigo aux lattes du lit

les volets à ce tableau ignoble

puis étirer encore des tapis

boucher les ouvertures

tendre par-dessus tout ça

le rideau imperméable de ta douche

et on aura un abri décent

en plein cœur du salon

#homesweethome

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}voilà

on est protégé·es du vent

protégé·es des regards

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ça fait du bien

d'être couché·es

contre ton corps

d'écouter les grenouilles

#ASMR

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ça fait toujours ça

les pluies violentes

quand on est au chaud

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et la nuit

sans douceur

commence à tomber

#seeyoutomorrow

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp17.jpg}
```
# 17. Visibilité

c'est beaucoup mieux

la tempête nous assiège

mais au moins on est protégé·es

des regards de la ville

#blindtest

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}dans ce gros désordre

qui n'aura plus jamais

une gueule d'appartement

tu ne te sens pas observé·e

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est un soulagement

que tu partages

avec le reste de notre génération

on ne se sent pas observé·es

#magicmirror

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et pour s'assurer

qu'on barbote tranquillement

le pouvoir liquide

s'est trouvé des maîtres-nageurs

avec des capes d'invisibilité

#pottermore

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}paradoxalement

l'objectif à court terme

c'est toujours d'être visible

les politiques doivent être visibles

les entreprises doivent être visibles

les produits doivent être visibles

être visible est même devenu

un job à part entière

#showyourbeauty

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les rois ne voyaient pas grand-chose

quand un peu par hasard

ils attrapaient un·e criminel·le

ils l'affichaient

dans le tonnerre du spectacle

pendaisons dans les champs

écartèlements sur les marchés

décapitations devant le château

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les états modernes

sont des entreprises qui ont réussi

alors ils ont pris leurs distances

avec les orages

#waterproof

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ils ont caché les corps

tué à l'abri des regards

érigé des prisons

où l'on se sait observé·e

ils ont disséminé

partout et en plein jour

ces petites caméras

noires et sphériques

fondé le contrôle

sur la surveillance

comme feeling

#vintage

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}maintenant

les cyberpouvoirs

tuent le feeling

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}l'observation disparaît

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors

soudain angoissé·e

l'observé·e se montre

#showmustgoon

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le feeling sous contrainte

ne suffit plus

pour nous dicter notre conduite

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on s'est habitué·es à tout voir

habitué·es à être vu·es

on hurle

dans tous les canaux de streaming

pour accéder enfin au silence

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}à chaque mutation du contrôle

on découvre une nouvelle esquive

une autre manière de voir

une autre manière d'être vu·e

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}chaque résistance

invente une soumission

mais aucune goutte d'eau

ne fait déborder la piscine

#fiftyshadesofgrey

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp18.jpg}
```
# 18. Marécages

il doit être à peu près minuit

plus grand-chose ne donne l'heure

tu sais te repérer

avec les étoiles ?

nous non plus

#lost

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}au moins il reste des clopes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on pourrait s'aventurer

en dehors du refuge

pour en partager une

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on voudrait voir un peu

quelle gueule il a

le monde dehors

#openspace

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la tempête est partie

le déluge a laissé derrière lui

une bruine diffuse

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les grenouilles qu'on entendait

à l'intérieur de l'abri

en fait elles sont des dizaines

elles règnent sans partage

sur nos marécages suspendus

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}impossible de fumer cette clope

sans marcher dans l'eau

sans écarter ces roseaux

qui nous écorchent les jambes

#neverquit

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}si t'avais des bottes de pluie

on sauterait dans les flaques

comme les batraciennes créatives

qu'il faudra bien devenir

si on veut continuer

d'habiter ton appartement

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est absurde

on dit ton appartement

mais c'est plus qu'un rectangle

de béton sans parois

au sommet d'un immeuble

#roomwithaview

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}une tourbière

de quarante mètres carrés

posée en haut d'une tour

avec une cabane au milieu

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}devant nous le néant

même les ultrasons que l'on émet

s'égarent dans l'horizon

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tant de satellites

que le ciel s'étoile

de lueurs un peu électriques

un peu minérales

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quelques goélands viennent cueillir

les grenouilles dissidentes

qui nous sautent entre les jambes

ils doivent nicher pas loin

quand les oiseaux mangent

quand les oiseaux font l'amour

le sol tremble un peu

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}marche pas aussi près du bord

s'il te plaît

on te l'a déjà dit

tout est glissant

si tu veux t'approcher du vide

accroupis-toi et rampe

nos ailes galèrent à pousser

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}par contre

nos bouches ont muté

#duckface

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}d'un coup de langue extensible

ultrarapide et hyperréaliste

on gobe les moustiques

et les mouches

comme les batraciennes créatives

qu'il a bien fallu devenir

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}voilà

on a dévoré les lucioles

on entend mieux les drones

ils sont devenus

plus faciles à repérer

plus faciles à abattre

#huntingseason

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp19.jpg}
```
# 19. Données

c'est vrai que l'appartement éventré

découpe quatre frontières nettes

mais les limites de la piscine

se sont évaporées

avec les limites du monde

et il n'y a pas de limites

à l'économie de la donnée

parce que le monde

n'a pas de limites

#freemium

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}depuis environ

la période où on est né·es

le monde a commencé

à produire une copie de lui-même

toujours perfectible

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et la copie n'a pas de limites

#follow4follow

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le monde a toujours été

un empilement de variables

mais on avait pas encore envisagé

à l'échelle de sapiens

qu'un alignement de nombres

puisse devenir habitable

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est qu'il n'existe aucun phénomène

qui ne puisse devenir donnée

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}plus la surface du réel

sera dure à habiter

plus on posera des capteurs

qui peuvent donner la vie

plus il fera bon vivre

à l'intérieur de la copie

#instagood

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et comme il faut bien

gérer les données

la totalité des phénomènes

l'ensemble de la vie comme moments

est devenu gérable

#dailyroutine

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors là

c'est la grosse euphorie

c'est la piscine olympique

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}partout un désir ardent

de devenir un objet

ou juste le reflet d'un objet

sur une paroi du cloud

partout un désir ardent

mais jamais incendiaire

de devenir silicone

#shadows

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}comme une intuition collective

à se former en gestionnaire

pas pour maîtriser la copie

la copie restera souveraine

juste y collaborer

accéder au bonheur

de construire ensemble

toujours plus de données

bosser en Californie

ou dans les Californies du coin

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}adopter des algorithmes

comme animaux de compagnie

devenir du même coup

animaux de compagnie

pour algorithmes

#petlovers

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu sais plus comment vivre

sans que tout soit augmenté

c'est une hallucination que tu partages

avec le reste de notre génération

on sait plus comment vivre

sans que tout soit augmenté

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais tu sais

la copie ne flotte pas dans les airs

ses pieds sont en argile

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}des groupes d'individus

trop mobiles pour être décalqués

pourraient nager entre ses jambes

et remonter chacun des fils

de la grande toile de danger

qu'a tissé l'Occident

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}apnée entre les câbles

poumons électroniques

cagoules multicolores

et briquets dans les poches

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}un court-circuit incendiaire

et tout le coton du *cloud*

fumerait comme la laine

des moutons électriques

#bedtimesstories

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}un court-circuit incendiaire

et toute la Silicon Valley

exhalerait une douce odeur

de silicium fondu

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ça vraiment

ce serait un spectacle

qui mériterait

de devenir une société

#happyending

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp20-date-4.jpg}
```
# 20. Goélands

```{=tex}
\begin{datation}

juin 2019

410.53 ppm de CO\textsubscript{2}

\vspace*{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les goélands de ce fleuve

qui ne s'appelle pas encore la Seine

nichent sur les pierres

dans les herbes hautes

qui peuplent les marécages

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}des marécages qui nous précèdent

des marécages qui étaient là

cinq mille ans avant notre naissance

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}de là où ils sont

cinq mille ans avant notre naissance

les goélands voient bien la plaine

le fleuve qui la cisèle

et nourrit toute la horde

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}la plaine est vide

parfaitement habitable

et des générations d'oiseaux

y appareillent sans entrave

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}puis la plaine est envahie

les premières habitations

agglutinées le long du fleuve

hébergent des primates innocent·es

qui courent après les lion·nes

et finissent presque toujours

par se faire dévorer

```{=tex}
\vspace{1\baselineskip}
```
les goélands continuent la chasse

mais leur cœur est lourd

en gaélique

gouelan signifie « pleurer »

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quand des foyers

s'allument dans les cabanes

les goélands s'approchent

nichent sur les toits

et gardent leurs œufs au chaud

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}au fil des siècles

les oiseaux tentent de coexister

déplacent encore leur nid

réinventent la prédation

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les cabanes changent de dimension

et les donjons de pierre

érigés sur la plaine

deviennent des perchoirs sans danger

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}depuis leurs hauteurs fortifiées

les goélands regardent

calmement

les volées de flèches

qui passent en contrebas

les boulets de fonte

qui s'échouent dans le fleuve

tout le grondement des guerres

mutilant la plaine

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}puis les usines

commencent à fumer

et intoxiquent les oisillons

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors leurs parents trouvent refuge

sous les toits prolétaires

et commencent à théoriser

la fin de la coopération

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout au long du siècle

les balles perdues

tirées sur les manifestant·es

qui occupent les fabriques

explosent par erreur

des œufs innocents

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les nids sont encore déplacés

encore reconstruits

toujours temporaires

toujours dérisoires

mais toujours autonomes

et toujours défendus

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}finalement la capitale commence

à produire une copie d'elle-même

et la copie de la capitale

n'a pas de limites

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les opérateurs de la copie

sont les drones de la police

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ils commencent

à scanner les carrefours

à filmer les impasses

à déranger l'atmosphère

où habitent les oiseaux

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors toute l'espèce décide

que la paix a trop duré

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les goélands comprennent

que les drones s'envolent

d'étranges nids électroniques

lovés dans les commissariats

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les drones s'appellent tous Elsa

Engins Légers de Surveillance Aérienne

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les goélands ne peuvent pas voir

les télépilotes qui guident Elsa

dans ses vols d'observation

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les télépilotes voient tout

mais jamais personne

n'a vu un·e télépilote

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}Elsa survole les rues

Elsa explore les impasses

fabrique la carte

des meutes actionnées

des agitations sous contrôle

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}Elsa est légère

c'est dans son nom

elle fluidifie

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les régulateurs

aiment déréguler

mais les gestionnaires

détestent la congestion

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}Elsa décongestionne

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors quelque chose vient toucher

ce qui touchait sans toucher

quelque chose de très énervé

une colère de plumes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les goélands se cachent sous les toits

guettent les drones

et quand une proie passe

s'envolent d'un même élan

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le vent porte leurs membres

ils sont plus souples

plus habiles que les engins

leurs becs détruisent les câbles

et leurs pattes arrachent

les ailes ridicules des robots

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les goélands organisé·es

ont entrepris méthodiquement

de crever les yeux d'Elsa

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}de là où t'es

tu ne vois que les chutes

les carcasses en polymère

de fonctionnaires électroniques abattus

que d'autres fonctionnaires ramassent

avec une moue dépitée

et dans le regard

une once de regret budgétaire

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quand le matin viendra

Elsa ne rentrera pas au nid

vaincue par les géants

qui l'empêchent de marcher

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}pour le moment

```{=tex}
\end{datation}
```
```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp21.jpg}
```
# 21. Suicide

regarde

encore un drone qui vole

#safari

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu sais

cette résistance ovipare

contre les robots policiers

elle a défrayé les journaux parisiens

pendant quelques mois

mais on a embelli l'histoire

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}depuis la police a développé

des drones antigoélands

qui diffusent des ultrasons

pour faire fuir les oiseaux

et ça les rend fous

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ils n'auront pas le temps de réagir

on peut se raconter ce qu'on veut

l'espèce évolue moins vite

que les dispositifs sécuritaires

#slowmotion

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est normal

que tu sois fatigué·e

il doit être trois heures du matin

on devrait essayer de dormir

#restless

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu recommences

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on t'a déjà demandé

on t'a déjà supplié en fait

de t'éloigner du bord

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on sait ce que t'as en tête

on n'a pas envie de le dire

et t'as pas envie d'en parler

mais il va falloir en parler

#voidcore

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}en tout cas

il va falloir arrêter de se taire

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on va plus pouvoir ignorer

ce visage impassible

que t'affiches en permanence

et qui colle pas à la situation

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on est désolé·es

on est vraiment désolé·es

tu sais pas quoi dire

ça oui

on peut le comprendre

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}t'as vu

ces points lumineux là-bas

on dirait des feux de camp

peut-être d'autres meutes

peut-être d'autres appartements

ou d'anciennes maisons bourgeoises

effondrées mais lumineuses

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu penses que les autres

iels ont muté comme nous ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}si tu veux avoir la réponse

il faudrait que demain

tu sois encore parmi nous

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}t'as vraiment plus assez de curiosité

pour encore une journée sur terre ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}s'il te plaît

te lève pas encore une fois

reste assis·e

peut-être

qu'on n'est pas assez clair·es

ce qu'on voudrait

c'est t'entourer d'un amour

qui nous torde ensemble

et torde avec nous

tout le spectre du réel

#gravity

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ce qu'on voudrait

c'est regarder avec toi

le monde au premier degré

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on a lu quelque part

même si c'est probablement faux

que 1968 serait l'année

ayant connu

le taux de suicide le plus bas

en Europe occidentale

#motivationalquote

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les émeutes rachètent l'avenir

mais c'est pas seulement les émeutes

les émeutes c'est très accessoire

c'est l'espace

pour inventer autre chose

la perspective

de la fin des jours présents

#reset

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on voit bien que c'est trop vague

pour gonfler de sens

le réel autour de toi

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}d'accord

oui tu peux te lever

pardon

on te suppliera plus

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on te croit

on te prend au sérieux

on va venir avec toi

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}s'asseoir tout près du bord

et balancer nos jambes

au-dessus du vide

#onedge

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on t'a vu te faire attraper

cet après-midi

au milieu des fumigènes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ça fait un moment

qu'on a compris

la lassitude dans ton visage

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ton sursis va sauter

et cette fois

tu vas prendre du ferme

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on peut fuir ensemble

on est prêt·es à faire ça

encore quelques heures

et nos ailes auront poussé

#wingsuit

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}t'es sûr·e ?

si c'est vraiment ce que tu veux

on t'en empêchera pas

on détournera pas les yeux

ça on te le promet

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais on trouve quand même

que tu devrais hésiter plus que ça

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}est-ce qu'on peut

te serrer contre nous ?

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}oui on pleure

mais ça tu vas devoir vivre avec

enfin

partir avec

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est l'image que t'emmèneras

en plongeant dans le vide

#tearsintherain

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on peut pas te prendre au sérieux

tout en retenant nos larmes

c'est politiquement incompatible

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est ça le premier degré

l'impression définitive

de ne plus être en train de jouer

#gamechanger

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp22.jpg}
```
# 22. Jeu

bien sûr

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}après 1968

on a redécouvert

les vertus du second degré

comme armure

comme maillot de bain

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et depuis

posé·es sur nos matelas gonflables

flottant sans histoire

un soda à la main

on garde nos lunettes de soleil

pour bien fixer

le feu des nuages

#swag

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et plus on réalise

la préfabrication générale

plus notre second degré se développe

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il y a corrélation des deux courbes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout nous amuse

tout nous divertit

#lol

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}même l'histoire

on a du mal

à la prendre au sérieux

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}elle est devenue

une image-objet

un spectacle-machine

comme le reste

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}même nos dépressions

même nos burn-outs

on a du mal

à les prendre au sérieux

on les soigne à la plage

on fait des pauses

qui ont le goût du kérosène

on continue à jouer

on joue à s'épuiser

et on soigne l'épuisement

avec les outils des épuiseurs

#workplacewellness

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}si seulement

on arrivait à faire un break

avec le besoin de faire un break

#haveakitkat

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}les vieux antagonismes de classes

n'ont pas disparu

ils ont juste été modélisés

dans un jeu vidéo permanent

et comme dans n'importe quel jeu

pour gagner la partie

il faut intérioriser l'algorithme

#actorstudio

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}notre génération apprend

la grande compétition du monde

en faisant l'expérience des règles

qui gouvernent la piscine

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}notre génération

apprend que ces règles

sont arbitraires

et sans fondement

qu'on peut les utiliser

avec cynisme et brutalité

pour aller plus loin dans la partie

#workhardplayhard

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout est devenu un jeu

qu'on a trop conscience de jouer

et plus on a conscience de jouer

plus notre second degré se développe

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il y a corrélation des deux courbes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on fait semblant

d'être solides

semblant d'en avoir rien à foutre

mais c'est pour survivre

à l'arbitraire

#newskill

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}là s'ouvre l'ironocène

l'époque du second degré général

reconductible et illimité

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}dans notre superpiscine

c'est notre superpouvoir :

il n'existe rien

qu'on ne puisse tourner en dérision

#trollface

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}« vis dans ton monde

joue dans le nôtre »

c'est le slogan qu'ils ont choisi

pour vendre la playstation

mais ils auraient pu choisir

« joue dans ton monde,

vis dans le nôtre »

et tout aurait été parfaitement identique

#whatever

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}le plus gros défi

de notre génération

c'est de garder son sérieux

c'est de prendre à nouveau

le monde au premier degré

#trynottolaugh

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors c'est vrai que c'est chiant

d'avoir grandi

en détestant nos possibles

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais quand on te voit

marcher vers nous

depuis le bord du précipice

où t'as refusé de sauter

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quand on te voit éteindre ta clope là

et sourire

on a envie de pleurer

et cette fois

c'est ni la beuh

ni les lacrymos

c'est vraiment la joie

c'est l'instant

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}t'as déjà vu quelqu'un

pleurer au second degré ?

#nofilter

#forreal

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp23.jpg}
```
# 23. Meute

finalement

on n'aura pas dormi une seconde

le soleil se lève

#welcomeback

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tu vois

le monde a changé de gueule

et tu ne tousses plus

les lacrymos ont fini

par déserter ta gorge

#healthy

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}maintenant

il te faut une bonne douche

juste pour nettoyer

les derniers résidus du gaz irritant

et préparer ton envol

tes ailes ont presque fini de pousser

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}regarde la ville entamer sa mue

toute la surface du réel

redevenue intéressante

#goldenhour

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on dirait bien

que tous les appartements

ont connu le même sort

que celui qui t'appartenait

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quand on sera descendu·es en ville

ou envolé·es dans les airs

on ne reconnaîtra jamais le tien

on ne retrouvera plus jamais

le chemin de la maison

#digitalnomad

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}plus rien n'est spectaculaire

le monde n'est pas effondré

l'humanité n'est pas éteinte

il n'y a qu'Hollywood

pour faire fortune

sur l'apocalypse

sur le spectacle de la fin

et la paralysie du public

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout lutte simplement

pour changer sans trop souffrir

tout lutte simplement

pour réduire la souffrance

c'est la composante

la plus révolutionnaire

du vivant mutilé

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est le dernier moment

qu'on voudrait partager

qu'on voudrait vivre à tes côtés

juste regarder le monde

au premier degré

sortir de l'obsession

du retournement

prendre au sérieux

la possibilité du retournement

#hashtaghashtag

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}et sous le soleil de ce matin

c'est plus facile que d'habitude

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}un truc dans nos membres

qui évacue les toxines

un truc qu'on pourrait appeler

le contraire de la passivité

un truc qui résiste

à l'image de la transformation générale

à l'extinction comme spectacle

#detox

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}un truc qui aide à envisager

la journée de combat

comme un horizon satisfaisant

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tiens

c'est étrange

on croirait entendre

quelqu'un frapper à la porte

#cliffhanger

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp24.jpg}
```
# 24. Recommencer

on a halluciné

il n'y a plus de porte

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}mais il y a bien une silhouette

qui se découpe

dans la lueur du levant

raide comme une matraque

figée dans ce qu'il reste

de ton appartement

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est un officier de police

et le flingue à la ceinture

de son uniforme antiémeute

est blanc de calcaire

#ancientstatue

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}son gros bouclier en plexiglas

est couvert de larves

ses bottes trop lourdes

s'enfoncent dans la tourbière

son uniforme trop serré

peine à contenir

la fourrure épaisse

qui doit lui recouvrir le corps

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ces deux croûtes

juste sous ses clavicules

ce sont des branchies

qu'il a grattées jusqu'au sang

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}impossible de voir ses yeux

jusqu'à ce qu'il ouvre sa visière

libère une flaque d'eau croupie

et quelques algues

qui lui pendent

comme une frange ridicule

du front jusqu'au larynx

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}à en croire son regard

on dirait presque

qu'il se trouve encore

adapté à la situation

#lame

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il brandit devant lui

un bout de papier trempé

pas besoin de s'approcher

il n'y a qu'à voir son sourire

c'est un mandat d'arrestation

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}il te demande

ta carte d'identité

et tu lui réponds que la marée

l'a emportée très loin

#messageinabottle

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}même les menottes rouillées

qu'il passe à tes poignets

n'effacent pas ton sourire

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}du haut de la tour

on te regarde monter

dans la fourgonnette

garée sur le trottoir

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}de là où on est

on s'en veut de ne rien faire

on voudrait

sauter dans le vide

replier nos jambes

contre nos poitrines

et se laisser tomber

de tout notre poids

#biggersplash

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}une dernière bombe

dans la piscine

juste éclabousser la gueule

des fonctionnaires cyniques

qui t'emmènent loin de nous

#boom

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors on s'approche du bord

et on décide de le faire

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on saute

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on replie nos jambes

contre nos poitrines

mais quelques mètres avant

de nous écraser

sur le toit du fourgon

on déploie nos ailes

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}toi tu lèves la tête vers nous

tu nous regardes planer

tu nous fais signe de la main

et tu sais qu'on se reverra

#cheesy

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}alors c'est vrai

que c'est toujours pareil

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}toute l'énergie qu'on met

pour faire tourner le vent

pour changer le sens

du chantier

pour poser quelques infrastructures

qui nous appartiennent vraiment

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}toute l'énergie qu'on met

à maçonner des entailles

dans l'évidence

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}tout ça pour finir

avec les yeux rouges

du gaz dans la gorge

et des barreaux à nos fenêtres

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}ça donne envie de prendre l'air

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}quand on voit

la douceur du carnage

on n'est pas sûr·es

que ça mérite

de devenir une société

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}c'est un doute

dont on ne se séparera jamais

avec le reste de notre génération

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}on n'est pas certain·es

d'avoir un avenir

parce qu'on n'est pas certain·es

d'être à la hauteur

suffisamment sérieux·ses

suffisamment empathiques

pour faire face

pour se sécher les épaules

étendre la nappe sur l'herbe

raviver les braises

mais pour incendier seulement

l'extrémité d'une clope

```{=tex}
\vspace{1\baselineskip}
```
`\textcolor{red}{===}`{=tex}attendre enfin en paix

la capitulation de la Terre

et l'extinction-soleil

#over

```{=tex}
\cleardoubleevenpage
\includepdf[width=118mm,height=185mm]{./gabarit/illustrations/xp25.jpg}
```
